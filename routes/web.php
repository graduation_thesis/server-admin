<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('{any}', function () {
    return view('welcome');
})->where('any', '.*');

Route::get('excel', 'CheckController@showExport');
Route::get('xuatfile', 'CheckController@download')->name('export.list');

Route::get('check-unit', 'CheckController@checkunit');
