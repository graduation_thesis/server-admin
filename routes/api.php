<?php

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Kiểm tra đăng nhập
Route::group(['prefix' => 'auth'], function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
});

// check logout
Route::group([
    'prefix' => 'auth',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('logout', 'AuthController@logout');
        $router->get('me', 'AuthController@me');
    });
});

// Tổng quan
Route::group([
    'prefix' => 'home',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'HomeController@index');
    });
});

// Tài khoản người sử dụng
Route::group([
    'prefix' => 'user',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'UserController@index');
        $router->get('getPaginate', 'UserController@getPaginate');
        $router->get('isActive', 'UserController@isActive');
        $router->post('create', 'UserController@create');
        $router->get('show/{id}', 'UserController@show');
        $router->post('update/{id}', 'UserController@update');
        $router->delete('remove/{id}', 'UserController@remove');
        $router->post('searchAll', 'UserController@searchAll');

        // Tình trạng tài khoản
        $router->post('checkActive/{id}/{active}', 'UserController@checkActive');
    });
});

// Số quyết định
Route::group([
    'prefix' => 'decision',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'DecisionController@index');
        $router->get('getPaginate', 'DecisionController@getPaginate');
        $router->post('create', 'DecisionController@create');
        $router->get('show/{id}', 'DecisionController@show');
        $router->post('update/{id}', 'DecisionController@update');
        $router->delete('remove/{id}', 'DecisionController@remove');
        $router->post('upload', 'DecisionController@upload');
        $router->post('searchAll', 'DecisionController@searchAll');
        $router->post('searchOptions', 'DecisionController@searchOptions');
    });
});

// Văn bản
Route::group([
    'prefix' => 'document',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'DocumentController@index');
        $router->post('create', 'DocumentController@create');
        $router->get('show/{id}', 'DocumentController@show');
        $router->post('update/{id}', 'DocumentController@update');
        $router->delete('remove/{id}', 'DocumentController@remove');
        $router->post('upload', 'DocumentController@upload');
        $router->post('searchAll', 'DocumentController@searchAll');
    });
});

// Cơ quan quyết định
Route::group([
    'prefix' => 'decisionAgency',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'DecisionAgencyController@index');
        $router->post('create', 'DecisionAgencyController@create');
        $router->get('show/{id}', 'DecisionAgencyController@show');
        $router->post('update/{id}', 'DecisionAgencyController@update');
        $router->delete('remove/{id}', 'DecisionAgencyController@remove');
    });
});

// Chuyển công tác cá nhân
Route::group([
    'prefix' => 'changePersonal',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'ChangePersonalController@index');
        $router->post('create', 'ChangePersonalController@create');
        $router->delete('remove/{id}', 'ChangePersonalController@remove');
        $router->post('searchAll', 'ChangePersonalController@searchAll');
    });
});

// Chuyển công tác tập thể đơn vị
Route::group([
    'prefix' => 'changeCollective',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'ChangeCollectiveController@index');
        $router->post('create', 'ChangeCollectiveController@create');
        $router->delete('remove/{id}', 'ChangeCollectiveController@remove');
        $router->post('searchAll', 'ChangeCollectiveController@searchAll');
    });
});

// Danh hiệu cá nhân
Route::group([
    'prefix' => 'personalTitle',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'PersonalTitleController@index');
        $router->post('create', 'PersonalTitleController@create');
        $router->get('show/{id}', 'PersonalTitleController@show');
        $router->post('update/{id}', 'PersonalTitleController@update');
        $router->delete('remove/{id}', 'PersonalTitleController@remove');
    });
});

// Danh hiệu tập thể
Route::group([
    'prefix' => 'collectiveTitle',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'CollectiveTitleController@index');
        $router->post('create', 'CollectiveTitleController@create');
        $router->get('show/{id}', 'CollectiveTitleController@show');
        $router->post('update/{id}', 'CollectiveTitleController@update');
        $router->delete('remove/{id}', 'CollectiveTitleController@remove');
    });
});

// Hình thức khen thưởng
Route::group([
    'prefix' => 'formsOfReward',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'FormsOfRewardController@index');
        $router->post('create', 'FormsOfRewardController@create');
        $router->get('show/{id}', 'FormsOfRewardController@show');
        $router->post('update/{id}', 'FormsOfRewardController@update');
        $router->delete('remove/{id}', 'FormsOfRewardController@remove');
    });
});

// Hình thức kỷ luật
Route::group([
    'prefix' => 'formsOfDiscipline',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'FormsOfDisciplineController@index');
        $router->post('create', 'FormsOfDisciplineController@create');
        $router->get('show/{id}', 'FormsOfDisciplineController@show');
        $router->post('update/{id}', 'FormsOfDisciplineController@update');
        $router->delete('remove/{id}', 'FormsOfDisciplineController@remove');
    });
});

// Hình thức rủi ro
Route::group([
    'prefix' => 'formsRisk',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->post('create', 'FormsRiskController@create');
        $router->get('list', 'FormsRiskController@index');
        $router->get('show/{id}', 'FormsRiskController@show');
        $router->post('update/{id}', 'FormsRiskController@update');
        $router->delete('remove/{id}', 'FormsRiskController@remove');
    });
});

// Đơn vị trên cơ sở
Route::group([
    'prefix' => 'school',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->post('create', 'SchoolController@create');
        $router->get('list', 'SchoolController@index');
        $router->get('show/{id}', 'SchoolController@show');
        $router->post('update/{id}', 'SchoolController@update');
        $router->delete('remove/{id}', 'SchoolController@remove');
        $router->get('export', 'SchoolController@export');
    });
});

// Options add team
Route::get('school/getUnits/{id}', 'UnitController@getUnits');
Route::get('unit/bundle/{id}', 'UnitController@bundle');

// Options add staff
Route::get('unit/getTeams/{id}', 'TeamController@getTeams');
Route::get('team/bundle/{id}', 'TeamController@bundle');

// Options Add Account
Route::get('unit/getStaffs/{id}', 'StaffController@getStaffs');
Route::get('staff/bundle/{id}', 'StaffController@bundle');

// Đơn vị cơ sở
Route::group([
    'prefix' => 'unit',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'UnitController@index');
        $router->get('getPaginate', 'UnitController@getPaginate');
        $router->post('create', 'UnitController@create');
        $router->get('show/{id}', 'UnitController@show');
        $router->post('update/{id}', 'UnitController@update');
        $router->delete('remove/{id}', 'UnitController@remove');
        $router->post('searchAll', 'UnitController@searchAll');
    });
});

// Đơn vị trực thuộc (Tổ, Đội)
Route::group([
    'prefix' => 'team',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'TeamController@index');
        $router->get('getPaginate', 'TeamController@getPaginate');
        $router->post('create', 'TeamController@create');
        $router->get('show/{id}', 'TeamController@show');
        $router->post('update/{id}', 'TeamController@update');
        $router->delete('remove/{id}', 'TeamController@remove');
        $router->post('searchAll', 'TeamController@searchAll');
    });
});

// Cán bộ
Route::group([
    'prefix' => 'staff',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'StaffController@index');
        $router->get('getPaginate', 'StaffController@getPaginate');
        $router->post('create', 'StaffController@create');
        $router->get('show/{id}', 'StaffController@show');
        $router->post('update/{id}', 'StaffController@update');
        $router->delete('remove/{id}', 'StaffController@remove');
        $router->post('import', 'StaffController@import');
        $router->get('export', 'StaffController@export');
        $router->post('searchAll', 'StaffController@searchAll');
        $router->get('profile/{id}', 'StaffController@profile');
        // TEST DOWNLOAD EXCEL
//        $router->get('export/{id}', 'StaffController@download');
    });
});

Route::get('staff/export/{id}', 'StaffController@download'); // vì reload lại out auth nên bỏ ngoài



// Demo để check file excel khi export vue sang laravel export
Route::get('xuat', 'CheckController@export')->name('export.xuat');

// Loại Cán bộ
Route::group([
    'prefix' => 'typeStaff',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'TypeStaffController@index');
        $router->post('create', 'TypeStaffController@create');
        $router->get('show/{id}', 'TypeStaffController@show');
        $router->post('update/{id}', 'TypeStaffController@update');
        $router->delete('remove/{id}', 'TypeStaffController@remove');
    });
});

// Cấp bậc Cán bộ
Route::group([
    'prefix' => 'rankStaff',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'RankStaffController@index');
        $router->post('create', 'RankStaffController@create');
        $router->get('show/{id}', 'RankStaffController@show');
        $router->post('update/{id}', 'RankStaffController@update');
        $router->delete('remove/{id}', 'RankStaffController@remove');
    });
});

// Chức vụ Cán bộ
Route::group([
    'prefix' => 'positionStaff',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'PositionStaffController@index');
        $router->post('create', 'PositionStaffController@create');
        $router->get('show/{id}', 'PositionStaffController@show');
        $router->post('update/{id}', 'PositionStaffController@update');
        $router->delete('remove/{id}', 'PositionStaffController@remove');
    });
});

// Đăng kí thi đua cá nhân
Route::group([
    'prefix' => 'registerReward',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'RegisterRewardController@index');
        $router->delete('remove/{id}', 'RegisterRewardController@remove');
        $router->get('getListChecked', 'RegisterRewardController@getListChecked');
        $router->post('checked/{id}/{checker}', 'RegisterRewardController@checked');
        $router->post('checkedAll/{id}/{checker}', 'RegisterRewardController@checkedAll');
        $router->post('searchAll', 'RegisterRewardController@searchAll');
        $router->post('searchOptions', 'RegisterRewardController@searchOptions');
    });
});

// Đăng kí thi đua đơn vị
Route::group([
    'prefix' => 'registerCollectiveReward',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'RegisterCollectiveRewardController@index');
        $router->delete('remove/{id}', 'RegisterCollectiveRewardController@remove');
        $router->get('getListChecked', 'RegisterCollectiveRewardController@getListChecked');
        $router->post('checked/{id}/{checker}', 'RegisterCollectiveRewardController@checked');
        $router->post('checkedAll/{id}/{checker}', 'RegisterCollectiveRewardController@checkedAll');
        $router->post('searchAll', 'RegisterCollectiveRewardController@searchAll');
        $router->post('searchOptions', 'RegisterCollectiveRewardController@searchOptions');
    });
});

// Đăng kí thi đua cá nhân đã thông qua kiểm duyệt
Route::group([
    'prefix' => 'checkedReward',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'CheckedRewardController@index');
        $router->post('create', 'CheckedRewardController@create');
        $router->get('show/{id}', 'CheckedRewardController@show');
        $router->post('update/{id}', 'CheckedRewardController@update');
        $router->delete('remove/{id}', 'CheckedRewardController@remove');
        $router->post('searchAll', 'CheckedRewardController@searchAll');
        $router->post('searchOptions', 'CheckedRewardController@searchOptions');
        $router->post('import', 'CheckedRewardController@import');
        $router->get('export', 'CheckedRewardController@export');
    });
});

// Đăng kí thi đua đơn vị đã thông qua kiểm duyệt
Route::group([
    'prefix' => 'checkedCollectiveReward',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'CheckedCollectiveRewardController@index');
        $router->post('create', 'CheckedCollectiveRewardController@create');
        $router->get('show/{id}', 'CheckedCollectiveRewardController@show');
        $router->post('update/{id}', 'CheckedCollectiveRewardController@update');
        $router->delete('remove/{id}', 'CheckedCollectiveRewardController@remove');
        $router->post('searchAll', 'CheckedCollectiveRewardController@searchAll');
        $router->post('searchOptions', 'CheckedCollectiveRewardController@searchOptions');
        $router->post('import', 'CheckedCollectiveRewardController@import');
        $router->get('export', 'CheckedCollectiveRewardController@export');
    });
});

// Kết quả thi đua cá nhân
Route::group([
    'prefix' => 'personalRewardResult',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'PersonalRewardResultController@index');
        $router->post('create', 'PersonalRewardResultController@create');
        $router->get('show/{id}', 'PersonalRewardResultController@show');
        $router->post('update/{id}', 'PersonalRewardResultController@update');
        $router->delete('remove/{id}', 'PersonalRewardResultController@remove');
        $router->post('searchAll', 'PersonalRewardResultController@searchAll');
        $router->post('searchOptions', 'PersonalRewardResultController@searchOptions');
        $router->post('import', 'PersonalRewardResultController@import');
        $router->get('export', 'PersonalRewardResultController@export');
    });
});

// Kết quả thi đua đơn vị
Route::group([
    'prefix' => 'collectiveRewardResult',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'CollectiveRewardResultController@index');
        $router->post('create', 'CollectiveRewardResultController@create');
        $router->get('show/{id}', 'CollectiveRewardResultController@show');
        $router->post('update/{id}', 'CollectiveRewardResultController@update');
        $router->delete('remove/{id}', 'CollectiveRewardResultController@remove');
        $router->post('searchAll', 'CollectiveRewardResultController@searchAll');
        $router->post('searchOptions', 'CollectiveRewardResultController@searchOptions');
        $router->post('import', 'CollectiveRewardResultController@import');
        $router->get('export', 'CollectiveRewardResultController@export');
    });
});

// Thống kê Khen thưởng cá nhân
Route::group([
    'prefix' => 'personalReward',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'PersonalRewardController@index');
        $router->post('create', 'PersonalRewardController@create');
        $router->get('show/{id}', 'PersonalRewardController@show');
        $router->post('update/{id}', 'PersonalRewardController@update');
        $router->delete('remove/{id}', 'PersonalRewardController@remove');
        $router->post('searchAll', 'PersonalRewardController@searchAll');
        $router->post('searchOptions', 'PersonalRewardController@searchOptions');
        $router->post('import', 'PersonalRewardController@import');
        $router->get('chart', 'PersonalRewardController@chart');
    });
});

// Thống kê Khen thưởng tập thể
Route::group([
    'prefix' => 'collectiveReward',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'CollectiveRewardController@index');
        $router->post('create', 'CollectiveRewardController@create');
        $router->get('show/{id}', 'CollectiveRewardController@show');
        $router->post('update/{id}', 'CollectiveRewardController@update');
        $router->delete('remove/{id}', 'CollectiveRewardController@remove');
        $router->post('searchAll', 'CollectiveRewardController@searchAll');
        $router->post('searchOptions', 'CollectiveRewardController@searchOptions');
        $router->post('import', 'CollectiveRewardController@import');
        $router->get('chart', 'CollectiveRewardController@chart');
    });
});

// Thống kê rủi ro cán bộ
Route::group([
    'prefix' => 'riskDuty',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'RiskDutyController@index');
        $router->post('create', 'RiskDutyController@create');
        $router->get('show/{id}', 'RiskDutyController@show');
        $router->post('update/{id}', 'RiskDutyController@update');
        $router->delete('remove/{id}', 'RiskDutyController@remove');
        $router->post('searchAll', 'RiskDutyController@searchAll');
        $router->post('searchOptions', 'RiskDutyController@searchOptions');
        $router->get('chart', 'RiskDutyController@chart');
    });
});

// Thống kê số liệu
Route::group([
    'prefix' => 'statistical',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'StatisticalController@index');
        $router->post('searchOptions', 'StatisticalController@searchOptions');
    });
});

// Kỉ luật cá nhân
Route::group([
    'prefix' => 'discipline',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'DisciplineController@index');
        $router->post('create', 'DisciplineController@create');
        $router->get('show/{id}', 'DisciplineController@show');
        $router->post('update/{id}', 'DisciplineController@update');
        $router->delete('remove/{id}', 'DisciplineController@remove');
        $router->post('import', 'DisciplineController@import');
        $router->get('export', 'DisciplineController@export');
        $router->post('searchAll', 'DisciplineController@searchAll');
        $router->post('searchOptions', 'DisciplineController@searchOptions');
        $router->get('chart', 'DisciplineController@chart');
    });
});

// Thành tích cá nhân theo ID
Route::group([
    'prefix' => 'personalAchievements',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list/{id}', 'PersonalAchievementsController@list');
        $router->post('create', 'PersonalAchievementsController@create');
        $router->delete('remove/{id}', 'PersonalAchievementsController@remove');
        $router->post('searchAll', 'PersonalAchievementsController@searchAll');
    });
});

// Kỷ luật cá nhân theo ID
Route::group([
    'prefix' => 'personalDiscipline',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list/{id}', 'PersonalDisciplineController@list');
        $router->post('create', 'PersonalDisciplineController@create');
        $router->delete('remove/{id}', 'PersonalDisciplineController@remove');
        $router->post('searchAll', 'PersonalDisciplineController@searchAll');
    });
});

// Qúa trình chuyển công tác cá nhân theo ID
Route::group([
    'prefix' => 'changeStaff',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('show/{id}', 'ChangeStaffController@show');
        $router->post('update/{id}', 'ChangeStaffController@update');
    });
});

// Qúa trình chuyển công tác đơn vị cơ sở theo ID
//Route::group(['prefix' => 'changeUnit'], function () {
//    Route::get('show/{id}', 'ChangeUnitController@show');
//    Route::post('update/{id}', 'ChangeUnitController@update');
//});

// Qúa trình chuyển công tác
Route::group([
    'prefix' => 'moveUnit',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'MoveUnitController@index');
        $router->post('create', 'MoveUnitController@create');
        $router->delete('remove/{id}', 'MoveUnitController@remove');
    });
});

// Ý kiến phải hồi
Route::group([
    'prefix' => 'comment',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'CommentController@index');
        $router->post('create', 'CommentController@create');
        $router->get('show/{id}', 'CommentController@show');
        $router->post('update/{id}', 'CommentController@update');
        $router->delete('remove/{id}', 'CommentController@remove');
        $router->post('searchAll', 'CommentController@searchAll');
    });
});

// CHỨC NĂNG NÂNG CAO
Route::group([
    'prefix' => 'advanced',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->post('step1', 'AdvancedController@step1');
        $router->post('step2', 'AdvancedController@step2');
        $router->post('step3', 'AdvancedController@step3');
    });
});

// SO SÁNH BIỂU ĐỒ KHEN THƯỞNG - KỶ LUẬT
Route::group([
    'prefix' => 'compare',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('chart', 'CompareController@chart');
    });
});


//============================ Test\
// TEST BACKUP DATABASES
Route::get('backup/create', 'BackupController@create');

// TEST API MOBILE
// Đơn vị trên cơ sở
Route::group([
    'prefix' => 'mobile',
], function (Registrar $router) {
    $router->get('unit', 'UnitController@index');
    $router->get('school', 'SchoolController@index');
    $router->get('team', 'TeamController@index');
    $router->get('personalTitle', 'PersonalTitleController@index');
    $router->get('collectiveTitle', 'CollectiveTitleController@index');
    $router->get('formsOfReward', 'FormsOfRewardController@index');
    $router->get('formsOfDiscipline', 'FormsOfDisciplineController@index');
    $router->get('formsRisk', 'FormsRiskController@index');

//    $router->get('document', 'DocumentController@indexMobile');
    $router->get('document', 'DocumentController@index');

    $router->get('decision', 'DecisionController@index');
    $router->get('decisionAgency', 'DecisionAgencyController@index');
    $router->get('comment', 'CommentController@indexMobile');

    $router->get('chart', 'PersonalRewardController@chart');
    $router->get('chartDiscipline', 'DisciplineController@chart');
    $router->get('chartPersonalReward', 'PersonalRewardController@chart');
    $router->get('chartRiskDuty', 'RiskDutyController@chart');

    $router->get('user', 'UserController@index');
    $router->get('overview', 'HomeController@index');

});
