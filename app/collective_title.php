<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class collective_title extends Model
{
    protected $fillable = [
        'collective_title_id',
        'name',
        'release_date',
        'pending_remove',
    ];
}
