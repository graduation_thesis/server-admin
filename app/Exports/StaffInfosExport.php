<?php

namespace App\Exports;

use App\staff;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class StaffInfosExport implements FromQuery, WithTitle, WithHeadings, WithMapping
{
    private $code_number;

    public function __construct(string $code_number)
    {
        $this->code_number = $code_number;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return staff::query()->where('code_number', '=', $this->code_number);
    }

    // Check thử
    public function map($row): array
    {
        return [
//            $row->id,
//            $row->schools->name,
//            $row->units->name,
//            $row->team_id === null ? 'Hiện chưa tham gia' : $row->teams->name,
//            $row->types->name,
//            $row->ranks->name,
//            $row->positions->name,
//            $row->first_name,
//            $row->last_name,
//            $row->birthday,
//            $row->code_number,
//            $row->sex === 1 ? 'Nữ' : 'Nam' // o: nam ; 1: nữ
            $row->id,
            $row->school_id,
            $row->unit_id,
            $row->team_id === null ? '' : $row->team_id,
            $row->type_staff_id,
            $row->rank_staff_id,
            $row->position_staff_id,
            $row->full_name,
            $row->birthday,
            $row->code_number,
            $row->sex === 0 ? '0' : '1'
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'school_id',
            'unit_id',
            'team_id',
            'type_staff_id',
            'rank_staff_id',
            'position_staff_id',
            'full_name',
            'birthday',
            'code_number',
            'sex',
        ];
    }
    //END Check thử

    /**
     * @return string
     */
    public function title(): string
    {
        return 'staff_info';
    }
}
