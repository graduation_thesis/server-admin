<?php

namespace App\Exports;

use App\discipline;
use Maatwebsite\Excel\Concerns\FromCollection;

class DisciplinesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return discipline::all();
    }
}
