<?php

namespace App\Exports;

use App\personal_reward;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PersonalRewardInfosExport implements FromQuery, WithTitle, WithHeadings, WithMapping
{
    private $code_number;

    public function __construct(string $code_number)
    {
        $this->code_number  = $code_number;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return personal_reward::query()->where('code_number', '=', $this->code_number);

    }

    // Check thử
    public function map($row): array
    {
        return [
//            $row->id,
//            $row->personal_title_id === '' ? 'Không' : $row->personal_titles->name,
//            $row->forms_of_reward_id === '' ? 'Không' : $row->forms_of_rewards->name,
//            $row->decision_id,
//            $row->decision_agencies->name
            $row->id,
            $row->code_number,
            $row->school_id,
            $row->unit_id,
            $row->team_id,
//            $row->personal_title_id === '' ? '' : $row->personal_title_id,
            $row->forms_of_reward_id === '' ? '' : $row->forms_of_reward_id,
            $row->decision_id,
            $row->decision_agency_id,
            $row->reward_date
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'code_number',
            'school_id',
            'unit_id',
            'team_id',
//            'personal_title_id',
            'forms_of_reward_id',
            'decision_id',
            'decision_agency_id',
            'reward_date'
        ];
    }
    //END Check thử

    /**
     * @return string
     */
    public function title(): string
    {
        return 'personal_reward_staff_info';
    }
}
