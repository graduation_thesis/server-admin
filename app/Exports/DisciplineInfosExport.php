<?php

namespace App\Exports;

use App\discipline;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class DisciplineInfosExport implements FromQuery, WithTitle, WithHeadings, WithMapping
{
    private $code_number;

    public function __construct(string $code_number)
    {
        $this->code_number  = $code_number;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return discipline::query()->where('code_number', '=', $this->code_number);

    }

    public function map($row): array
    {
        return [
//            $row->id,
////            $row->forms_of_discipline_id === '' ? 'Không' : $row->forms_of_disciplines->name,
////            $row->decision_id,
////            $row->decision_agencies->name
///
                $row->id,
                $row->code_number,
                $row->school_id,
                $row->unit_id,
                $row->team_id,
                $row->forms_of_discipline_id === '' ? '' : $row->forms_of_discipline_id,
                $row->decision_id,
                $row->decision_agency_id,
                $row->discipline_date
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'code_number',
            'school_id',
            'unit_id',
            'team_id',
            'forms_of_discipline_id',
            'decision_id',
            'decision_agency_id',
            'discipline_date'
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'discipline_staff_info';
    }
}
