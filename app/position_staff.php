<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class position_staff extends Model
{
    //
    protected $fillable = [
        'position_staff_id',
        'name',
        'pending_remove',
    ];
}
