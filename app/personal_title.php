<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class personal_title extends Model
{
    //
    protected $fillable = [
        'personal_title_id',
        'name',
        'release_date',
        'pending_remove',
    ];
}
