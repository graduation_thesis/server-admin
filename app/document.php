<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class document extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'name',
        'document_file',
        'description',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'name',
        'document_file',
        'description',
        'pending_remove',
    ];
}
