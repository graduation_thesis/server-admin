<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class team extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'school_id',
        'unit_id',
        'name',
        'build_date',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'school_id',
        'unit_id',
        'name',
        'build_date',
        'pending_remove',
    ];

    public function schools()
    {
        return $this->belongsTo(school::class, 'school_id','school_id');
    }

    public function units()
    {
        return $this->belongsTo(unit::class, 'unit_id','unit_id');
    }
}
