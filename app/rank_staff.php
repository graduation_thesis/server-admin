<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rank_staff extends Model
{
    //
    protected $fillable = [
        'rank_staff_id',
        'name',
        'thumbnails',
        'pending_remove',
    ];
}
