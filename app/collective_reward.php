<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class collective_reward extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'school_id',
        'unit_id',
        'forms_of_reward_id',
        'decision_agency_id',
        'decision_id',
        'reward_date',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'school_id',
        'unit_id',
        'forms_of_reward_id',
        'decision_agency_id',
        'decision_id',
        'reward_date',
        'pending_remove',
    ];

    public function schools()
    {
        return $this->belongsTo(school::class, 'school_id','school_id');
    }

    public function units()
    {
        return $this->belongsTo(unit::class, 'unit_id','unit_id');
    }

    public function forms_of_rewards()
    {
        return $this->belongsTo(forms_of_reward::class, 'forms_of_reward_id','forms_of_reward_id');
    }

    public function decision_agencies()
    {
        return $this->belongsTo(decision_agency::class, 'decision_agency_id','decision_agency_id');
    }

    public function decisions()
    {
        return $this->belongsTo(decision::class, 'decision_id','decision_id');
    }
}
