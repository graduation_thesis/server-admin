<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class register_reward extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'user_id',
        'school_id',
        'unit_id',
        'code_number',
        'personal_title_id',
        'forms_of_reward_id',
        'decision_agency_id',
        'decision_id',
        'description',
        'reward_date',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'user_id',
        'school_id',
        'unit_id',
        'code_number',
        'personal_title_id',
        'forms_of_reward_id',
        'decision_agency_id',
        'decision_id',
        'description',
        'reward_date',
        'pending_remove',
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }

    public function schools()
    {
        return $this->belongsTo(school::class, 'school_id','school_id');
    }

    public function units()
    {
        return $this->belongsTo(unit::class, 'unit_id','unit_id');
    }

    public function ranks()
    {
        return $this->belongsTo(rank_staff::class, 'rank_staff_id','rank_staff_id');
    }

    public function staffs()
    {
        return $this->belongsTo(staff::class, 'code_number','code_number');
    }

    public function forms_of_rewards()
    {
        return $this->belongsTo(forms_of_reward::class, 'forms_of_reward_id','forms_of_reward_id');
    }

    public function personal_titles()
    {
        return $this->belongsTo(personal_title::class, 'personal_title_id','personal_title_id');
    }

    public function decision_agencies()
    {
        return $this->belongsTo(decision_agency::class, 'decision_agency_id','decision_agency_id');
    }

    public function decisions()
    {
        return $this->belongsTo(decision::class, 'decision_id','decision_id');
    }
}
