<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class risk_duty extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number',
        'school_id',
        'unit_id',
        'team_id',
        'forms_risk_id',
        'decision_agency_id',
        'decision_id',
        'risk_date',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'code_number',
        'school_id',
        'unit_id',
        'team_id',
        'forms_risk_id',
        'decision_agency_id',
        'decision_id',
        'risk_date',
        'pending_remove',
    ];

    public function staffs()
    {
        return $this->belongsTo(staff::class, 'code_number', 'code_number');
    }

    public function schools()
    {
        return $this->belongsTo(school::class, 'school_id','school_id');
    }

    public function units()
    {
        return $this->belongsTo(unit::class, 'unit_id','unit_id');
    }

    public function teams()
    {
        return $this->belongsTo(team::class, 'team_id','id');
    }

    public function forms_risks()
    {
        return $this->belongsTo(forms_risk::class, 'forms_risk_id','forms_risk_id');
    }

    public function decision_agencies()
    {
        return $this->belongsTo(decision_agency::class, 'decision_agency_id','decision_agency_id');
    }

    public function decisions()
    {
        return $this->belongsTo(decision::class, 'decision_id','decision_id');
    }
}
