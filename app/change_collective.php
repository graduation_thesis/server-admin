<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class change_collective extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'school_id',
        'unit_id',
        'unit_id_changed',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'school_id',
        'unit_id',
        'unit_id_changed',
        'pending_remove',
    ];

    public function schools()
    {
        return $this->belongsTo(school::class, 'school_id','school_id');
    }

    public function units()
    {
        return $this->belongsTo(unit::class, 'unit_id','unit_id');
    }

    public function new_units()
    {
        return $this->belongsTo(unit::class, 'unit_id_changed','unit_id');
    }
}
