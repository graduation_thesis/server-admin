<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class personal_reward_result extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'user_id',
        'code_number',
        'rank_staff_id',
        'school_id',
        'unit_id',
        'personal_title_id',
        'approved_by',
        'reward_date',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'user_id',
        'code_number',
        'rank_staff_id',
        'school_id',
        'unit_id',
        'personal_title_id',
        'approved_by',
        'reward_date',
        'pending_remove',
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }

    public function staffs()
    {
        return $this->belongsTo(staff::class, 'code_number','code_number');
    }

    public function ranks()
    {
        return $this->belongsTo(rank_staff::class, 'rank_staff_id','rank_staff_id');
    }

    public function schools()
    {
        return $this->belongsTo(school::class, 'school_id','school_id');
    }

    public function units()
    {
        return $this->belongsTo(unit::class, 'unit_id','unit_id');
    }

    public function personal_titles()
    {
        return $this->belongsTo(personal_title::class, 'personal_title_id','personal_title_id');
    }
}
