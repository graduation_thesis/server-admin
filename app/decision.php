<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class decision extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'decision_id',
        'decision_file',
        'sign_date',
        'received_date',
        'description',
        'pending_remove'
    ];

    protected $filter = [
        'id',
        'decision_id',
        'decision_file',
        'sign_date',
        'received_date',
        'description',
        'pending_remove'
    ];
}
