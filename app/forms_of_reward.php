<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class forms_of_reward extends Model
{
    //
    protected $fillable = [
        'forms_of_reward_id',
        'name',
        'pending_remove',
    ];
}
