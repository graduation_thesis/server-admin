<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class decision_agency extends Model
{
    //
    protected $fillable = [
        'decision_agency_id',
        'name',
    ];
}
