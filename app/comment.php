<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'user_id',
        'content',
        'feedback',
        'status'
    ];

    protected $filter = [
        'id',
        'user_id',
        'content',
        'feedback',
        'status'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }
}
