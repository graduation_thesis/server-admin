<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class discipline extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number',
        'school_id',
        'unit_id',
        'team_id',
        'forms_of_discipline_id',
        'decision_agency_id',
        'decision_id',
        'discipline_date',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'code_number',
        'school_id',
        'unit_id',
        'team_id',
        'forms_of_discipline_id',
        'decision_agency_id',
        'decision_id',
        'discipline_date',
        'pending_remove',
    ];

    public function staffs()
    {
        return $this->belongsTo(staff::class, 'code_number', 'code_number');
    }

    public function schools()
    {
        return $this->belongsTo(school::class, 'school_id','school_id');
    }

    public function units()
    {
        return $this->belongsTo(unit::class, 'unit_id','unit_id');
    }

    public function teams()
    {
        return $this->belongsTo(team::class, 'team_id','id');
    }

    public function forms_of_disciplines()
    {
        return $this->belongsTo(forms_of_discipline::class, 'forms_of_discipline_id','forms_of_discipline_id');
    }

    public function decision_agencies()
    {
        return $this->belongsTo(decision_agency::class, 'decision_agency_id','decision_agency_id');
    }

    public function decisions()
    {
        return $this->belongsTo(decision::class, 'decision_id','decision_id');
    }
}
