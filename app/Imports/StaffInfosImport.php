<?php

namespace App\Imports;

use App\staff;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class StaffInfosImport implements ToModel,WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new staff([
            //
            'school_id'             => $row['school_id'],
            'unit_id'               => $row['unit_id'],
            'team_id'               => $row['team_id'],
            'type_staff_id'         => $row['type_staff_id'],
            'rank_staff_id'         => $row['rank_staff_id'],
            'position_staff_id'     => $row['position_staff_id'],
            'full_name'             => $row['full_name'],
//            'birthday'              => $row['birthday'],
            'birthday'              => Date::excelToDateTimeObject($row['birthday']),
//            'birthday'              => Date::excelToDateTimeObject($row['birthday'])->format('Y-m-d'),
            'code_number'           => $row['code_number'],
            'sex'                   => $row['sex'],
            'pending_remove'        => null,
        ]);
    }
}
