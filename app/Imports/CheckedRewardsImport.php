<?php

namespace App\Imports;

use App\checked_reward;
use Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class CheckedRewardsImport implements ToModel,WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new checked_reward([
            //
//            'user_id'                       => Auth::user()->id,
            'user_id'                       => Auth::id(), // demo user id của admin
            'code_number'                   => $row['code_number'],
            'school_id'                     => $row['school_id'],
            'unit_id'                       => $row['unit_id'],
            'rank_staff_id'                 => $row['rank_staff_id'],
            'personal_title_id'             => $row['personal_title_id'],
            'reward_date'                   => Date::excelToDateTimeObject($row['reward_date']),
            'pending_remove'                => null,
        ]);
    }
//
//    public function headingRow(): int
//    {
//        return 7; // hàng sử dụng làm tiêu đề
//    }
}
