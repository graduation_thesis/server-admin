<?php

namespace App\Imports;

use App\collective_reward;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class CollectiveRewardsImport implements ToModel,WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new collective_reward([
            //
            'school_id'                 => 'DHCSND',
            'unit_id'                   => $row['unit_id'],
            'forms_of_reward_id'        => $row['forms_of_reward_id'],
            'decision_agency_id'        => $row['decision_agency_id'],
            'decision_id'               => $row['decision_id'],
            'reward_date'               => Date::excelToDateTimeObject($row['reward_date']),
            'pending_remove'            => null,
        ]);
    }
}
