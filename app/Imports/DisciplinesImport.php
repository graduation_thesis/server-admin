<?php

namespace App\Imports;

use App\discipline;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class DisciplinesImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new discipline([
            //
            'code_number'                   => $row['code_number'],
            'school_id'                     => $row['school_id'],
            'unit_id'                       => $row['unit_id'],
            'team_id'                       => $row['team_id'],
            'forms_of_discipline_id'        => $row['forms_of_discipline_id'],
            'decision_agency_id'            => $row['decision_agency_id'],
            'decision_id'                   => $row['decision_id'],
            'discipline_date'               => $row['discipline_date'],
            'discipline_date'               => Date::excelToDateTimeObject($row['discipline_date']),
            'pending_remove'                => null,
        ]);
    }
}
