<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class Step1sImport implements WithMultipleSheets
{
    public function sheets(): array
    {
        return [
            'staff_info' => new StaffInfosImport(),
            'personal_reward_staff_info' => new PersonalRewardsImport(),
            'discipline_staff_info' => new DisciplinesImport(),
        ];
    }
}
