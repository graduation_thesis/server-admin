<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class InvoicesImport implements WithMultipleSheets
{
    public function sheets(): array
    {
        // IMPORT NHIỀU SHEETS
        return [
            'staff_info' => new StaffInfosImport(),
            'personal_reward_staff_info' => new PersonalRewardsImport(),
            'discipline_staff_info' => new DisciplinesImport(),
        ];
    }
}
