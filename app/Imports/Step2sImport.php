<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class Step2sImport implements WithMultipleSheets
{
    public function sheets(): array
    {
        return [
            'staff_info' => new StaffInfosImport(),
            'personal_reward_staff_info' => new PersonalRewardsImport(),
        ];
    }
}
