<?php

namespace App\Imports;

use App\register_reward;
use Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class RegisterRewardsImport implements ToModel,WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new register_reward([
            //
            'user_id'                       => Auth::user()->id,
            'code_number'                   => $row['code_number'],
            'personal_title_id'             => $row['personal_title_id'],
            'forms_of_reward_id'            => $row['forms_of_reward_id'],
            'decision_agency_id'            => $row['decision_agency_id'],
            'decision_id'                   => $row['decision_id'],
            'description'                   => $row['description'],
            'reward_date'                   => Date::excelToDateTimeObject($row['reward_date']),
            'pending_remove'                => null,
        ]);
    }
}