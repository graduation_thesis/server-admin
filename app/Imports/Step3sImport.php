<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class Step3sImport implements WithMultipleSheets
{
    public function sheets(): array
    {
        return [
            'staff_info' => new StaffInfosImport(),
            'discipline_staff_info' => new DisciplinesImport(),
        ];
    }
}
