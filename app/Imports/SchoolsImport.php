<?php

namespace App\Imports;

use App\school;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class SchoolsImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new school([
            //
            'school_id'             => $row['school_id'],
            'name'                  => $row['name'],
            'build_date'            => Date::excelToDateTimeObject($row['build_date']),
            'pending_remove'        => null,
        ]);
    }
}
