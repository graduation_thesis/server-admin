<?php

namespace App\Imports;

use App\personal_reward_result;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class PersonalRewardResultsImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new personal_reward_result([
            //
            'user_id'                       => 1,
            'code_number'                   => $row['code_number'],
            'school_id'                     => $row['school_id'],
            'unit_id'                       => $row['unit_id'],
//            'forms_of_reward_id'        => $row['forms_of_reward_id'],
            'personal_title_id'            => $row['personal_title_id'] === '' ? null : $row['personal_title_id'],
            'decision_agency_id'            => $row['decision_agency_id'],
            'decision_id'                   => $row['decision_id'],
            'reward_date'                   => Date::excelToDateTimeObject($row['reward_date']),
            'pending_remove'                => null,
        ]);
    }
}
