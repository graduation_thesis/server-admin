<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class school extends Model
{
    protected $fillable = [
    'school_id',
    'name',
    'build_date',
    'pending_remove',
    ];

//    public function units()
//    {
//        return $this->hasMany(unit::class, 'school_id', 'school_id');
//    }
}
