<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class move_unit extends Model
{
    //
    public function staffs()
    {
        return $this->belongsTo(staff::class, 'staff_id');
    }

    public function units()
    {
        return $this->belongsTo(unit::class, 'unit_id');
    }
}
