<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class type_staff extends Model
{
    //
    protected $fillable = [
        'type_staff_id',
        'name',
        'pending_remove',
    ];
}
