<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class forms_risk extends Model
{
    protected $fillable = [
        'forms_risk_id',
        'name',
        'pending_remove',
    ];
}
