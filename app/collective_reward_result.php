<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class collective_reward_result extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'user_id',
        'school_id',
        'unit_id',
        'collective_title_id',
        'approved_by',
        'reward_date',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'user_id',
        'school_id',
        'unit_id',
        'collective_title_id',
        'approved_by',
        'reward_date',
        'pending_remove',
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }

    public function schools()
    {
        return $this->belongsTo(school::class, 'school_id','school_id');
    }

    public function units()
    {
        return $this->belongsTo(unit::class, 'unit_id','unit_id');
    }

    public function collective_titles()
    {
        return $this->belongsTo(collective_title::class, 'collective_title_id','collective_title_id');
    }
}
