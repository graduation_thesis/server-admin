<?php

namespace App;

use App\Support\DataTablePaginate;
//use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Cache;

//class User extends Authenticatable implements JWTSubject
class User extends Authenticatable
{
    use Notifiable, HasApiTokens;
    use DataTablePaginate;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code_number',
        'school_id',
        'unit_id',
        'username',
        'password',
        'role',
        'active'
    ];

    protected $filter = [
        'id',
        'code_number',
        'school_id',
        'unit_id',
        'username',
        'password',
        'role',
        'active'
    ];

    /**
     * Find the user instance for the given username.
     *
     * @param  string  $username
     * @return \App\User
     */
    public function findForPassport($username)
    {
        return $this->where('username', $username)->first();
    }

    public function staffs()
    {
        return $this->belongsTo(staff::class, 'code_number', 'code_number');
    }

    public function schools()
    {
        return $this->belongsTo(school::class, 'school_id','school_id');
    }

    public function units()
    {
        return $this->belongsTo(unit::class, 'unit_id','unit_id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = ['created_at', 'updated_at'];

    // Rest omitted for brevity

//    /**
//     * Get the identifier that will be stored in the subject claim of the JWT.
//     *
//     * @return mixed
//     */
//    public function getJWTIdentifier()
//    {
//        return $this->getKey();
//    }

//    /**
//     * Return a key value array, containing any custom claims to be added to the JWT.
//     *
//     * @return array
//     */
//    public function getJWTCustomClaims()
//    {
//        return [];
//    }
//
    public function isOnline() {
        return Cache::has('user-is-online' . $this->id);
    }
}
