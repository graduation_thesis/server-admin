<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthCreateRequest;
use App\User;
use Auth;
use Laravel\Passport\Client;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Lcobucci\JWT\Parser;

class AuthController extends AbstractApiController
{
    public function register(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $user = new User();
        $user->code_number = $request->code_number;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);

        if ($user->save()) {
            return response()->json([
                'message' => 'User tạo thành công!'
            ], 200);
        } else {
            return response()->json([
                'message' => 'User thất bại!'
            ], 500);
        }
    }

//    public function login(AuthCreateRequest $request) {
//
//        $validatedData = $request->validated();
//
//        if(!Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
//            $this->setMessage('Vui lòng kiểm tra lại tài khoản hoặc mật khẩu!');
//            $this->setStatus(401);
//            $this->setStatusCode(401);
//            return $this->respond();
//        }
//
//        $user = $request->user();
//
//        if($user->role == 'Administrator') {
//            $tokenData = $user->createToken('Personal Access Token', ['do_anything']);
//        } else {
//            $tokenData = $user->createToken('Personal Access Token', ['can_create']);
//        }
//
//        $token = $tokenData->token;
//
//        if($request->remember_me) {
//            $token->expires_at = Carbon::now()->addWeek(1);
//        }
//
//        if($token->save()) {
//            return response()->json([
//               'user' => $user,
//                'access_token' => $tokenData->accessToken,
//                'token_type' => 'Bearer',
//                'token_scope' => $tokenData->token->scopes[0],
//                'expires_at' => Carbon::parse($tokenData->token->expires_at)->toDateTimeString(),
//                'status_code' => 200
//            ], 200);
//        } else {
//            return response()->json([
//               'message' => 'Không hoạt động! làm ơn thử lại',
//                'status_code' => 500
//            ],500);
//        }
//    }

    public function login(AuthCreateRequest $request) {

        $passwordGrantClient = Client::where('password_client', 1)->first();

//        dd($passwordGrantClient);
            $data = [
                'grant_type' => 'password',
                'client_id' => $passwordGrantClient->id,
                'client_secret' => $passwordGrantClient->secret,
                'username' => $request->username,
                'password' => $request->password,
                'scope' => '*',
            ];

        $tokenRequest = Request::create('/oauth/token', 'post', $data);

        $tokenResponse = app()->handle($tokenRequest);
        $contentString = $tokenResponse->content();
        $tokenContent = json_decode($contentString, true);

        if(!empty($tokenContent['access_token'])) {
            return $tokenResponse;
        }

        return response()->json([
            'message' => 'Unauthenticated',
        ]);
    }

    public function logout(Request $request) {
//        $request->user()->tokens()->revoke();

        $request->user()->token()->revoke();

        return response()->json([
           'message' => 'Logout thành công',
           'status_code' => 200
        ]);
    }

    public function me() {
        return Auth::user();
    }
}
