<?php

namespace App\Http\Controllers;

use App\Exports\CheckedRewardExport;
use App\Exports\InvoicesExport;
use App\personal_reward;
use App\risk_duty;
use App\staff;
use App\unit;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CheckController extends Controller
{
    //
    public function check() {
        $books = personal_reward::with('staffs')->get();

//        foreach ($books as $book) {
//            echo $book->staff->rank;
//        }
        return $books;
    }

    public function showExport()
    {
        return view('home.index');
    }

    public function export()
    {
        return Excel::download(new CheckedRewardExport, 'invoices.xlsx', null, [], null);
    }

    public function download()
    {
        $code_number = '123-456';
        return (new InvoicesExport($code_number))->download('invoices.xlsx');
    }

    public function checkunit()
    {
        $staff = staff::query()->where('unit_id', '=', 'NN')->get();
//        $staff = staff::where('unit_id', '=', 'NN')->get();
//        dd($staff);
        return view('home.changeUnit', [
            'staff' => $staff
        ]);
    }
}
