<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;

class RegisterController extends Controller
{
    protected $auth;

    public function __construct(JWTAuth $auth)
    {
        $this->auth = $auth;
    }

    public function register(Request $request)
    {
        $user = User::create([
            'code_number' => $request['code_number'],
            'username' => $request['username'],
            'password' => bcrypt($request['password']),
            'post_privileged' => $request['post_privileged'],
            'post_privileged_granted_at' => $request['post_privileged_granted_at'],
            'active' => $request['active']
        ]);

        $token = $this->auth->attempt($request->only('username', 'password'));
        return response()->json([
           'meta' => [
             'authenticated' => true,
               'token' => $token
           ],
            'data' => [
                'user' => $user
            ]
        ], 200);
    }
}
