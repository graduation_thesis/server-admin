<?php

namespace App\Http\Controllers;

use App\Http\Requests\PersonalTitleCreateRequest;
use App\personal_title;
use Illuminate\Support\Facades\DB;

class PersonalTitleController extends AbstractApiController
{
    //
    public function index()
    {
        return response()->json(personal_title::get(), 200);
    }

    public function create(PersonalTitleCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['personal_title_id'] = $validatedData['personal_title_id'];
        $payload['name'] = $validatedData['name'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại danh hiệu');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu đơn vị trên cơ sở
        $personal_title = personal_title::create($payload);
        DB::beginTransaction();

        try {
            $personal_title->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm danh mục thành công!');
            $this->setStatusCode(200);
            $this->setData($personal_title);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($personal_title_id)
    {
        return personal_title::findOrFail($personal_title_id);
    }

    public function update(PersonalTitleCreateRequest $request, $personal_title_id)
    {
        $validatedData = $request->validated();

        $personal_title = personal_title::query()->findOrFail($personal_title_id);
        if (! $personal_title) {
            $this->setMessage('Không có danh hiệu cá nhân này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật danh hiệu cá nhân
                $personal_title->personal_title_id              = $validatedData['personal_title_id'];
                $personal_title->name                           = $validatedData['name'];

                $personal_title->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($personal_title);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        personal_title::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $personal_title = personal_title::query()->get();
        foreach ($personal_title->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }
}
