<?php

namespace App\Http\Controllers;

use App\Http\Requests\RankStaffCreateRequest;
use App\rank_staff;
use Illuminate\Support\Facades\DB;

class RankStaffController extends AbstractApiController
{
    //
    public function index()
    {
        return response()->json(rank_staff::get(), 200);
    }

    public function create(RankStaffCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['rank_staff_id'] = $validatedData['rank_staff_id'];
        $payload['name'] = $validatedData['name'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên cấp bậc');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu đơn vị trên cơ sở
        $rank_staff = rank_staff::create($payload);
        DB::beginTransaction();

        try {
            $rank_staff->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm cấp bậc thành công!');
            $this->setStatusCode(200);
            $this->setData($rank_staff);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($rank_staff_id)
    {
        return rank_staff::findOrFail($rank_staff_id);
    }

    public function update(RankStaffCreateRequest $request, $rank_staff_id)
    {
        $validatedData = $request->validated();

        $rank_staff = rank_staff::query()->findOrFail($rank_staff_id);
        if (! $rank_staff) {
            $this->setMessage('Không có cấp bậc này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên cấp bậc
                $rank_staff->rank_staff_id          = $validatedData['rank_staff_id'];
                $rank_staff->name                   = $validatedData['name'];

                $rank_staff->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($rank_staff);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        rank_staff::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $rank_staff = rank_staff::query()->get();
        foreach ($rank_staff->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }
}
