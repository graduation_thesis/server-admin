<?php

namespace App\Http\Controllers;

use App\Http\Requests\PersonalRewardCreateRequest;
use App\Imports\PersonalRewardsImport;
use App\personal_reward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PersonalRewardController extends AbstractApiController
{
    public function index(Request $request)
    {
        $personal_reward = personal_reward::query()
            ->select([
                'id',
                'code_number',
                'school_id',
                'unit_id',
                'team_id',
                'forms_of_reward_id',
                'decision_agency_id',
                'decision_id',
                'reward_date',
            ])
            ->with('staffs',
                'schools',
                'units',
                'teams',
                'forms_of_rewards',
                'decision_agencies',
                'decisions')
            ->DataTablePaginate($request);

        return $this->item($personal_reward);
    }

    public function chart()
    {
        $personal_reward = personal_reward::all();

        $grouped = $personal_reward->groupBy(function ($item, $key) {
            return substr($item['reward_date'], 0, 4);
        });

        $groupCount = $grouped->map(function ($item, $key) {
            return collect($item)->count();
        });

        return $this->item($groupCount);
    }


    public function create(PersonalRewardCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        foreach ($request['code_number'] as $item) {
            $payload['code_number'] = $item['value'];
            $payload['school_id'] = $item['valueSchoolId'];
            $payload['unit_id'] = $item['valueUnitId'];
            $payload['team_id'] = $item['valueTeamId'];

            if (($validatedData['forms_of_reward_id']) !== "null") {
                $payload['forms_of_reward_id']      = $validatedData['forms_of_reward_id'];
            }
            $payload['decision_agency_id'] = $validatedData['decision_agency_id'];
            $payload['decision_id'] = $validatedData['decision_id'];
            $payload['reward_date'] = $validatedData['reward_date'];

            // Tạo và lưu thành tích cá nhân
        $personal_reward = personal_reward::create($payload);
        DB::beginTransaction();

            try {
                $personal_reward->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm thành tích cá nhân thành công!');
                $this->setStatusCode(200);
                $this->setData($personal_reward);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function show($id)
    {
        return personal_reward::with('staffs',
            'schools',
            'units',
            'teams',
            'forms_of_rewards',
            'decision_agencies',
            'decisions')->findOrFail($id);
    }

    public function update(PersonalRewardCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $personal_reward = personal_reward::query()->findOrFail($id);
        if (! $personal_reward) {
            $this->setMessage('Không có việc khen thưởng cán bộ này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật khen thưởng cán bộ
                $personal_reward->code_number                        = $validatedData['code_number'];

                if (($validatedData['forms_of_reward_id']) !== "null") {
                    $personal_reward->forms_of_reward_id              = $validatedData['forms_of_reward_id'];
                } else {
                    $personal_reward->forms_of_reward_id              = null;
                }
                $personal_reward->decision_agency_id                 = $validatedData['decision_agency_id'];
                $personal_reward->decision_id                        = $validatedData['decision_id'];
                $personal_reward->reward_date                        = $validatedData['reward_date'];

                $personal_reward->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($personal_reward);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        personal_reward::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $personal_reward = personal_reward::query()
            ->select([
                'id',
                'code_number',
                'school_id',
                'unit_id',
                'team_id',
                'forms_of_reward_id',
                'decision_agency_id',
                'decision_id',
                'reward_date',
            ])
            ->with('staffs',
                'schools',
                'units',
                'teams',
                'forms_of_rewards',
                'decision_agencies',
                'decisions')
            ->whereHas('staffs', function($staffs) use($search) {
                $staffs->where('full_name', 'LIKE', "%$search%");
            })
            ->orWhereHas('units', function($units) use($search) {
                $units->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('forms_of_rewards', function($forms_of_rewards) use($search) {
                $forms_of_rewards->where('name', 'LIKE', "%$search%");
            })
            ->DataTablePaginate($request);
        return $this->item($personal_reward);
    }

    public function searchOptions(Request $request)
    {
        $searchForms = $request->keyOptionFormsOfReward;
        $start= $request->sign_date;
        $end = $request->received_date;

        if($searchForms && !$start && !$end) {
            $personal_reward = personal_reward::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->where('forms_of_reward_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($personal_reward);
        }
        else if($start && $end && !$searchForms) {
            $personal_reward = personal_reward::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->DataTablePaginate($request);
            return $this->item($personal_reward);
        }
        else if($searchForms && !$start && !$end) {
            $personal_reward = personal_reward::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->where('forms_of_reward_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($personal_reward);
        }
        else if ($searchForms && $start && $end) {
            $personal_reward = personal_reward::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->where('forms_of_reward_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($personal_reward);
        }
        else if (!$searchForms && $start && $end) {
            $personal_reward = personal_reward::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->DataTablePaginate($request);
            return $this->item($personal_reward);
        }
        else if ($searchForms && $start && $end) {
            $personal_reward = personal_reward::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->where('forms_of_reward_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($personal_reward);
        }
        else {
            $personal_reward = personal_reward::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->DataTablePaginate($request);
            return $this->item($personal_reward);
        }
    }

    public function import()
    {
        Excel::import(new PersonalRewardsImport, request()->file('file'));

        return redirect('/')->with('success', 'All good!');
    }
}
