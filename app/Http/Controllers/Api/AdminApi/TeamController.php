<?php

namespace App\Http\Controllers;

use App\Http\Requests\TeamCreateRequest;
use App\team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamController extends AbstractApiController
{
    public function index()
    {
        $team = team::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'name',
                'build_date'
            ])
            ->with('schools', 'units')
            ->get();

        return $this->item($team);
    }

    public function getPaginate(Request $request)
    {
        $team = team::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'name',
                'build_date'
            ])
            ->with('schools', 'units')
            ->DataTablePaginate($request);

        return $this->item($team);
    }


    public function getTeams($id)
    {
        $team = team::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'name',
                'build_date',
            ])
            ->where('unit_id', '=', $id)
            ->get();

        return $this->item($team);
    }

    public function bundle($id)
    {
        $query = team::query();
        $query->select([
            'id',
            'school_id',
            'unit_id',
            'name',
            'build_date'
        ]);
        $query->where('unit_id', '=', $id);

        $team = $query->firstOrFail();

        return $this->item($team);
    }

    public function create(TeamCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['school_id'] = $validatedData['school_id'];
        $payload['unit_id'] = $validatedData['unit_id'];
        $payload['name'] = $validatedData['name'];
        $payload['build_date'] = $validatedData['build_date'];

        // Tạo và lưu đơn vị trên cơ sở
        $team = team::create($payload);
        DB::beginTransaction();

        try {
            $team->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm đơn vị cơ sở thành công!');
            $this->setStatusCode(200);
            $this->setData($team);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return team::findOrFail($id);
    }

    public function update(TeamCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $team = team::query()->findOrFail($id);
        if (! $team) {
            $this->setMessage('Không có đội, đồn, tổ này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật danh hiệu cá nhân
                $team->school_id                    = $validatedData['school_id'];
                $team->unit_id                      = $validatedData['unit_id'];
                $team->name                         = $validatedData['name'];
                $team->build_date                   = $validatedData['build_date'];

                $team->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($team);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        team::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $team = team::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'name',
                'build_date'
            ])
            ->with('schools', 'units')
            ->whereHas('schools', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('units', function($forms_of_rewards) use($search) {
                $forms_of_rewards->where('name', 'LIKE', "%$search%");
            })
            ->orWhere('name', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($team);
    }
}
