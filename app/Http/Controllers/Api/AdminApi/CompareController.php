<?php

namespace App\Http\Controllers;

use App\discipline;
use App\personal_reward;
use Illuminate\Http\Request;

class CompareController extends AbstractApiController
{
    public function chart()
    {
        $discipline = discipline::all();

        $grouped = $discipline->groupBy(function ($item, $key) {
            return substr($item['discipline_date'], 0, 4);
        });

        $groupCount = $grouped->map(function ($item, $key) {
            return collect($item)->count();
        });

        $personalReward = personal_reward::all();

        $groupedReward = $personalReward->groupBy(function ($item, $key) {
            return substr($item['reward_date'], 0, 4);
        });

        $groupCountReward = $groupedReward->map(function ($item, $key) {
            return collect($item)->count();
        });

        $arrYes = [];

        foreach ($groupCountReward as $key1=>$value1) {
            foreach ($groupCount as $key2=>$value2) {
                if($key1 == $key2) {
                    array_push($arrYes, [
                        'getKey' => $key1,
                        'getVal1' => $value1,
                        'getVal2' => $value2
                    ]);
                }
            }
        }

        foreach ($arrYes as $key => $row) {
            $count[$key] = $row['getKey'];
        }
        array_multisort($count, SORT_ASC, $arrYes);

        return $this->item($arrYes);
    }
}
