<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\Step1sImport;
use App\Imports\Step2sImport;
use App\Imports\Step3sImport;

class AdvancedController extends AbstractApiController
{
    // staff - personal reward - discipline ===> import excel
    public function step1()
    {
        Excel::import(new Step1sImport, request()->file('file'));

        return redirect('/')->with('success', 'All good!');
    }

    // staff - personal reward ===> import excel
    public function step2()
    {
        Excel::import(new Step2sImport, request()->file('file'));

        return redirect('/')->with('success', 'All good!');
    }

    // staff - discipline ===> import excel
    public function step3()
    {
        Excel::import(new Step3sImport, request()->file('file'));

        return redirect('/')->with('success', 'All good!');
    }
}
