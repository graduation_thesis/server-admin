<?php

namespace App\Http\Controllers;

use App\collective_reward;
use App\discipline;
use App\forms_of_reward;
use App\personal_reward;
use App\risk_duty;
use Illuminate\Http\Request;

class StatisticalController extends AbstractApiController
{
    public function searchOptions(Request $request)
    {
        $start= $request->sign_date;
        $end = $request->received_date;

            $collective_reward = collective_reward::query()
                ->select([
                    'id',
                    'unit_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'created_at',
                ])
                ->with('units',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->get();

        $personal_reward = personal_reward::query()
            ->select([
                'id',
                'code_number',
                'forms_of_reward_id',
                'decision_agency_id',
                'decision_id',
                'created_at',
            ])
            ->with('staffs',
                'forms_of_rewards',
                'decision_agencies',
                'decisions')
            ->whereHas('decisions', function($decisions) use($start, $end) {
                $decisions->whereBetween('received_date', [$start, $end]);
            })
            ->get();

        $discipline = discipline::query()
            ->select([
                'id',
                'code_number',
                'school_id',
                'unit_id',
                'team_id',
                'forms_of_discipline_id',
                'decision_agency_id',
                'decision_id',
                'created_at',
            ])
            ->with('staffs',
                'schools',
                'units',
                'teams',
                'forms_of_disciplines',
                'decision_agencies',
                'decisions')
            ->whereHas('decisions', function($decisions) use($start, $end) {
                $decisions->whereBetween('received_date', [$start, $end]);
            })
            ->get();

        $risk_duty = risk_duty::query()
            ->select([
                'id',
                'code_number',
                'school_id',
                'unit_id',
                'team_id',
                'forms_risk_id',
                'decision_agency_id',
                'decision_id',
                'created_at',
            ])
            ->with('staffs',
                'schools',
                'units',
                'teams',
                'forms_risks',
                'decision_agencies',
                'decisions')
            ->whereHas('decisions', function($decisions) use($start, $end) {
                $decisions->whereBetween('received_date', [$start, $end]);
            })
            ->get();

        return $this->item([$collective_reward, $personal_reward, $discipline, $risk_duty]);
    }
}