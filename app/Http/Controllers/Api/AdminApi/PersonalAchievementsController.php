<?php

namespace App\Http\Controllers;

use App\Http\Requests\PersonalAchievementsCreateRequest;
use App\Http\Requests\PersonalRewardCreateRequest;
use App\personal_reward;
use App\staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PersonalAchievementsController extends AbstractApiController
{
    //
    public function list(Request $request, $id)
    {
        $personal_reward = personal_reward::query()
            ->select([
                'id',
                'code_number',
                'school_id',
                'unit_id',
                'team_id',
//                'personal_title_id',
                'forms_of_reward_id',
                'decision_agency_id',
                'decision_id',
                'created_at',
                'reward_date'
            ])
            ->with('staffs',
                'forms_of_rewards',
//                'personal_titles',
                'decision_agencies',
                'decisions')
            ->where('code_number', '=', $id)
            ->DataTablePaginate($request);
        return $this->item($personal_reward);
    }

    public function create(PersonalAchievementsCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['code_number']                         = $validatedData['code_number'];

        $getList = staff::query()->where('code_number', '=', $payload['code_number'])->get();

        $payload['school_id']                           = $getList[0]['school_id'];
        $payload['unit_id']                             = $getList[0]['unit_id'];
        $payload['team_id']                             = $getList[0]['team_id'];
//        $payload['personal_title_id']                   = $validatedData['personal_title_id'];
        $payload['forms_of_reward_id']                  = $validatedData['forms_of_reward_id'];
        $payload['decision_agency_id']                  = $validatedData['decision_agency_id'];
        $payload['decision_id']                         = $validatedData['decision_id'];
        $payload['reward_date']                         = $validatedData['reward_date'];

        // Tạo và lưu thành tích cá nhân
        $personal_reward = personal_reward::create($payload);
        DB::beginTransaction();

        try {
            $personal_reward->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm thành tích cá nhân thành công!');
            $this->setStatusCode(200);
            $this->setData($personal_reward);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function remove($id)
    {
        personal_reward::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $personal_reward = personal_reward::query()
            ->select([
                'id',
                'code_number',
//                'personal_title_id',
                'forms_of_reward_id',
                'decision_agency_id',
                'decision_id',
                'created_at',
                'reward_date'
            ])
            ->with('staffs',
                'schools',
                'units',
                'teams',
                'forms_of_rewards',
//                'personal_titles',
                'decision_agencies',
                'decisions')
            ->WhereHas('forms_of_rewards', function($forms_of_rewards) use($search) {
                $forms_of_rewards->where('name', 'LIKE', "%$search%");
            })
//            ->orWhereHas('personal_titles', function($staffs) use($search) {
//                $staffs->where('name', 'LIKE', "%$search%");
//            })
            ->orWhereHas('decision_agencies', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhere('decision_id', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($personal_reward);
    }
}
