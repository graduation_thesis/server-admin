<?php

namespace App\Http\Controllers;

use App\checked_collective_reward;
use App\checked_reward;
use App\Exports\CheckedRewardExport;
use App\Http\Requests\CheckedCollectiveRewardCreateRequest;
use App\Http\Requests\CheckedRewardCreateRequest;
use App\Imports\CheckedRewardsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class CheckedCollectiveRewardController extends AbstractApiController
{
    public function index(Request $request)
    {
        $checked_collective_reward = checked_collective_reward::query()
            ->select([
                'id',
                'user_id',
                'school_id',
                'unit_id',
                'collective_title_id',
                'reward_date',
            ])
            ->with('users',
                'schools',
                'units',
                'collective_titles')
            ->DataTablePaginate($request);

        return $this->item($checked_collective_reward);
    }


    public function create(CheckedCollectiveRewardCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        foreach ($request['unit_id'] as $item) {
            $payload['user_id']                         = $validatedData['user_id'];
            $payload['school_id']                       = $item['valueSchoolId'];
            $payload['unit_id']                         = $item['value'];
            $payload['collective_title_id']             = $validatedData['collective_title_id'];
            $payload['approved_by']                     = $validatedData['user_id'];
            $payload['reward_date']                     = $validatedData['reward_date'];

            // Tạo và lưu thành tích cá nhân
            $checked_collective_reward = checked_collective_reward::create($payload);
            DB::beginTransaction();

            try {
                $checked_collective_reward->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm đăng kí thi đua thành công!');
                $this->setStatusCode(200);
                $this->setData($checked_collective_reward);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function show($id)
    {
        return checked_collective_reward::with('users',
            'schools',
            'units',
            'collective_titles')->findOrFail($id);
    }

    public function update(CheckedCollectiveRewardCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $checked_collective_reward = checked_collective_reward::query()->findOrFail($id);
        if (! $checked_collective_reward) {
            $this->setMessage('Không có việc đăng kí thi đua này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật khen thưởng đơn vị
                $checked_collective_reward->user_id                             = $validatedData['user_id'];
                $checked_collective_reward->school_id                           = $validatedData['school_id'];
                $checked_collective_reward->unit_id                             = $validatedData['unit_id'];
                $checked_collective_reward->collective_title_id                 = $validatedData['collective_title_id'];
                $checked_collective_reward->approved_by                         = $validatedData['approved_by'];
                $checked_collective_reward->reward_date                         = $validatedData['reward_date'];

                $checked_collective_reward->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($checked_collective_reward);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }


    public function remove($id)
    {
        checked_collective_reward::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $checked_collective_reward = checked_collective_reward::query()
            ->select([
                'id',
                'user_id',
                'school_id',
                'unit_id',
                'collective_title_id',
                'reward_date',
            ])
            ->with('users',
                'schools',
                'units',
                'collective_titles')
            ->whereHas('users', function($users) use($search) {
                $users->where('username', 'LIKE', "%$search%");
            })
            ->orWhereHas('schools', function($schools) use($search) {
                $schools->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('units', function($units) use($search) {
                $units->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('collective_titles', function($collective_titles) use($search) {
                $collective_titles->where('name', 'LIKE', "%$search%");
            })
            ->DataTablePaginate($request);
        return $this->item($checked_collective_reward);
    }

    public function searchOptions(Request $request)
    {
        $searchTitle = $request->keyOptionPersonalTitle;

        if($searchTitle) {
            $checked_collective_reward = checked_collective_reward::query()
                ->select([
                    'id',
                    'user_id',
                    'school_id',
                    'unit_id',
                    'collective_title_id',
                    'reward_date',
                ])
                ->with('users',
                    'schools',
                    'units',
                    'collective_titles')
                ->where('collective_title_id', '=', $searchTitle)
                ->DataTablePaginate($request);
            return $this->item($checked_collective_reward);
        } else {
            $checked_collective_reward = checked_collective_reward::query()
                ->select([
                    'id',
                    'user_id',
                    'school_id',
                    'unit_id',
                    'collective_title_id',
                    'reward_date',
                ])
                ->with('users',
                    'schools',
                    'units',
                    'collective_titles')
                ->DataTablePaginate($request);
            return $this->item($checked_collective_reward);
        }
    }

    public function import()
    {
        Excel::import(new CheckedRewardsImport, request()->file('file'));

        return redirect('/')->with('success', 'All good!');
    }

    public function export()
    {
        return Excel::download(new CheckedRewardExport, 'invoices.xlsx');
    }
}
