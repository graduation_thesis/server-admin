<?php

namespace App\Http\Controllers;

use App\checked_reward;
use App\Http\Requests\PersonalRewardResultCreateRequest;
use App\Imports\PersonalRewardResultsImport;
use App\personal_reward_result;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PersonalRewardResultController extends AbstractApiController
{
    public function index(Request $request)
    {
        $personal_reward_result = personal_reward_result::query()
            ->select([
                'id',
                'user_id',
                'school_id',
                'unit_id',
                'rank_staff_id',
                'code_number',
                'personal_title_id',
                'reward_date',
            ])
            ->with('users',
                'staffs',
                'schools',
                'units',
                'ranks',
                'personal_titles')
            ->DataTablePaginate($request);

        return $this->item($personal_reward_result);
    }


    public function create(PersonalRewardResultCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        foreach ($request['code_number'] as $item) {
            $payload['user_id']                     = $validatedData['user_id'];
            $payload['code_number']                 = $item['valueCodeNumber'];
            $payload['school_id']                   = $item['valueSchoolId'];
            $payload['unit_id']                     = $item['valueUnitId'];
            $payload['rank_staff_id']               = $item['valueRankId'];
            $payload['personal_title_id']           = $validatedData['personal_title_id'];
            $payload['approved_by']                 = $validatedData['user_id'];
            $payload['reward_date']                 = $validatedData['reward_date'];

            // Tạo và lưu thành tích cá nhân
            $personal_reward_result = personal_reward_result::create($payload);
            DB::beginTransaction();

            try {
                $personal_reward_result->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm kết quả thi đua thành công!');
                $this->setStatusCode(200);
                $this->setData($personal_reward_result);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function show($id)
    {
        return personal_reward_result::with('users',
            'staffs',
            'ranks',
            'schools',
            'units',
            'personal_titles')->findOrFail($id);
    }

    public function update(PersonalRewardResultCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $personal_reward_result = personal_reward_result::query()->findOrFail($id);
        if (! $personal_reward_result) {
            $this->setMessage('Không có kết quả thi đua này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật khen thưởng đơn vị
                $personal_reward_result->user_id                             = $validatedData['user_id'];
                $personal_reward_result->code_number                         = $validatedData['code_number'];
                $personal_reward_result->school_id                           = $validatedData['school_id'];
                $personal_reward_result->unit_id                             = $validatedData['unit_id'];
                $personal_reward_result->rank_staff_id                       = ! empty($validatedData['rank_staff_id']) ? $validatedData['rank_staff_id'] : null;
                $personal_reward_result->personal_title_id                   = $validatedData['personal_title_id'];
                $personal_reward_result->approved_by                         = $validatedData['approved_by'];
                $personal_reward_result->reward_date                         = $validatedData['reward_date'];

                $personal_reward_result->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($personal_reward_result);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }


    //////////////////////////////// Ở TRÊN ĐÃ FIX THEO KẾT QUẢ THI ĐUA
    /// Ở DƯỚI TỪ REMOVE THÌ VẪN CHƯA
    public function remove($id)
    {
        personal_reward_result::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $checked_reward = checked_reward::query()
            ->select([
                'id',
                'user_id',
                'school_id',
                'unit_id',
                'rank_staff_id',
                'code_number',
                'personal_title_id',
                'reward_date',
            ])
            ->with('users',
                'staffs',
                'ranks',
                'schools',
                'units',
                'personal_titles')
            ->whereHas('users', function($staffs) use($search) {
                $staffs->where('username', 'LIKE', "%$search%");
            })
            ->orWhereHas('schools', function($schools) use($search) {
                $schools->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('units', function($units) use($search) {
                $units->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('ranks', function($ranks) use($search) {
                $ranks->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('personal_titles', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhere('code_number', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($checked_reward);
    }

    public function searchOptions(Request $request)
    {
        $searchTitle = $request->keyOptionPersonalTitle;

        if($searchTitle) {
            $personal_reward_result = personal_reward_result::query()
                ->select([
                    'id',
                    'user_id',
                    'school_id',
                    'unit_id',
                    'rank_staff_id',
                    'code_number',
                    'personal_title_id',
                    'reward_date',
                ])
                ->with('users',
                    'staffs',
                    'ranks',
                    'schools',
                    'units',
                    'personal_titles')
                ->where('personal_title_id', '=', $searchTitle)
                ->DataTablePaginate($request);
            return $this->item($personal_reward_result);
        } else {
            $personal_reward_result = personal_reward_result::query()
                ->select([
                    'id',
                    'user_id',
                    'school_id',
                    'unit_id',
                    'rank_staff_id',
                    'code_number',
                    'personal_title_id',
                    'reward_date',
                ])
                ->with('users',
                    'staffs',
                    'ranks',
                    'schools',
                    'units',
                    'personal_titles')
                ->DataTablePaginate($request);
            return $this->item($personal_reward_result);
        }
    }

    public function import()
    {
        Excel::import(new PersonalRewardResultsImport, request()->file('file'));

        return redirect('/')->with('success', 'All good!');
    }
}
