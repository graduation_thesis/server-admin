<?php

namespace App\Http\Controllers;

use App\Http\Requests\TypeStaffCreateRequest;
use App\type_staff;
use Illuminate\Support\Facades\DB;

class TypeStaffController extends AbstractApiController
{
    public function index()
    {
        return response()->json(type_staff::get(), 200);
    }

    public function create(TypeStaffCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['type_staff_id'] = $validatedData['type_staff_id'];
        $payload['name'] = $validatedData['name'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên loại cán bộ');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu đơn vị trên cơ sở
        $type_staff = type_staff::create($payload);
        DB::beginTransaction();

        try {
            $type_staff->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm loại cán bộ thành công!');
            $this->setStatusCode(200);
            $this->setData($type_staff);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($type_staff_id)
    {
        return type_staff::findOrFail($type_staff_id);
    }

    public function update(TypeStaffCreateRequest $request, $type_staff_id)
    {
        $validatedData = $request->validated();

        $type_staff = type_staff::query()->findOrFail($type_staff_id);
        if (! $type_staff) {
            $this->setMessage('Không có loại cán bộ này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật danh hiệu cá nhân
                $type_staff->type_staff_id                  = $validatedData['type_staff_id'];
                $type_staff->name                           = $validatedData['name'];

                $type_staff->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($type_staff);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        type_staff::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $type_staff = type_staff::query()->get();
        foreach ($type_staff->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }
}
