<?php

namespace App\Http\Controllers;

use App\Http\Requests\PositionStaffCreateRequest;
use App\position_staff;
use Illuminate\Support\Facades\DB;

class PositionStaffController extends AbstractApiController
{
    //
    public function index()
    {
        return response()->json(position_staff::get(), 200);
    }

    public function create(PositionStaffCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['position_staff_id'] = $validatedData['position_staff_id'];
        $payload['name'] = $validatedData['name'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên chức vụ');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu chức vụ
        $position_staff = position_staff::create($payload);
        DB::beginTransaction();

        try {
            $position_staff->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm chức vụ thành công!');
            $this->setStatusCode(200);
            $this->setData($position_staff);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($position_staff_id)
    {
        return position_staff::findOrFail($position_staff_id);
    }

    public function update(PositionStaffCreateRequest $request, $position_staff_id)
    {
        $validatedData = $request->validated();

        $position_staff = position_staff::query()->findOrFail($position_staff_id);
        if (! $position_staff) {
            $this->setMessage('Không có chức vụ này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên cấp bậc
                $position_staff->position_staff_id          = $validatedData['position_staff_id'];
                $position_staff->name                       = $validatedData['name'];

                $position_staff->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($position_staff);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        position_staff::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $position_staff = position_staff::query()->get();
        foreach ($position_staff->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }
}
