<?php

namespace App\Http\Controllers;

use App\collective_reward;
use App\comment;
use App\decision;
use App\decision_agency;
use App\discipline;
use App\document;
use App\personal_reward;
use App\register_collective_reward;
use App\register_reward;
use App\staff;
use App\unit;
use App\User;
use Illuminate\Http\Request;

class HomeController extends AbstractApiController
{
    public function index()
    {
        $count_user                     = User::query()->count(); // Số lượng tài khoản
        $count_unit                     = unit::query()->count(); // Đơn vị cơ sở
        $count_staff                    = staff::query()->count(); // Cán bộ
        $count_comment                  = comment::query()->count(); // Ý kiến phản hồi
        $count_personal_reward          = personal_reward::query()->count(); // Khen thưởng cá nhân
        $count_collective_reward        = collective_reward::query()->count(); // Khen thưởng tập thể
        $count_discipline               = discipline::query()->count(); // Kỉ luật
        $count_personal_register        = register_reward::query()->count(); // Đăng kí thi đua cá nhân
        $count_collective_register      = register_collective_reward::query()->count(); // Đăng kí thi đua tập thể đơn vị
        $count_decision                 = decision::query()->count(); // Số quyết định
        $count_decision_agency          = decision_agency::query()->count(); // Số quyết định
        $count_document                 = document::query()->count(); // Số quyết định

        return $this->item([$count_user,
            $count_unit,
            $count_staff,
            $count_comment,
            $count_personal_reward,
            $count_collective_reward,
            $count_discipline,
            $count_personal_register,
            $count_collective_register,
            $count_decision,
            $count_decision_agency,
            $count_document
        ]);
    }
}
