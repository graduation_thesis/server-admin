<?php

namespace App\Http\Controllers;

use App\Http\Requests\StaffCreateRequest;
use App\staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChangeStaffController extends AbstractApiController
{
    public function show($id)
    {
        return staff::with('schools','units','teams','ranks','positions','types')->findOrFail($id);
    }

    public function update(StaffCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $staff = staff::query()->findOrFail($id);
        if (! $staff) {
            $this->setMessage('Không có cán bộ này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật cán bộ
                $staff->school_id                       = $validatedData['school_id'];
                $staff->unit_id                         = $validatedData['unit_id'];
                $staff->team_id                         = $validatedData['team_id'];
                $staff->full_name                       = $validatedData['full_name'];
                $staff->birthday                        = $validatedData['birthday'];
                $staff->sex                             = $validatedData['sex'];
                $staff->code_number                     = $validatedData['code_number'];
                $staff->type_staff_id                   = $validatedData['type_staff_id'];
                $staff->rank_staff_id                   = $validatedData['rank_staff_id'];
                $staff->position_staff_id               = $validatedData['position_staff_id'];

                $staff->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($staff);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
