<?php

namespace App\Http\Controllers;

use App\collective_reward;
use App\discipline;
use App\Exports\DisciplinesExport;
use App\Http\Requests\DisciplineCreateRequest;
use App\Imports\DisciplinesImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class DisciplineController extends AbstractApiController
{
    //
    public function index(Request $request)
    {
        $discipline = discipline::query()
            ->select([
                'id',
                'code_number',
                'school_id',
                'unit_id',
                'team_id',
                'forms_of_discipline_id',
                'decision_agency_id',
                'decision_id',
                'discipline_date',
            ])
            ->with('staffs',
                'schools',
                'units',
                'teams',
                'forms_of_disciplines',
                'decision_agencies',
                'decisions')
            ->DataTablePaginate($request);
        return $this->item($discipline);
    }

    public function chart()
    {
        $discipline = discipline::all();

        $grouped = $discipline->groupBy(function ($item, $key) {
            return substr($item['discipline_date'], 0, 4);
        });

        $groupCount = $grouped->map(function ($item, $key) {
            return collect($item)->count();
        });

        return $this->item($groupCount);
    }

    public function create(DisciplineCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        foreach ($request['code_number'] as $item) {
            $payload['code_number']                     = $item['value'];
            $payload['school_id']                       = $item['valueSchoolId'];
            $payload['unit_id']                         = $item['valueUnitId'];
            $payload['team_id']                         = $item['valueTeamId'];
            $payload['forms_of_discipline_id']          = $validatedData['forms_of_discipline_id'];
            $payload['decision_agency_id']              = $validatedData['decision_agency_id'];
            $payload['decision_id']                     = $validatedData['decision_id'];
            $payload['discipline_date']                 = $validatedData['discipline_date'];

            // Tạo và lưu kỷ luật cá nhân
            $discipline = discipline::create($payload);
            DB::beginTransaction();

            try {
                $discipline->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm kỷ luật cá nhân thành công!');
                $this->setStatusCode(200);
                $this->setData($discipline);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function show($id)
    {
        return discipline::with('staffs',
            'schools',
            'units',
            'teams',
            'forms_of_disciplines',
            'decision_agencies',
            'decisions')->findOrFail($id);
    }

    public function update(DisciplineCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $discipline = discipline::query()->findOrFail($id);
        if (! $discipline) {
            $this->setMessage('Không có việc kỷ luật này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật kỷ luật
                $discipline->code_number                        = $validatedData['code_number'];
                $discipline->forms_of_discipline_id             = $validatedData['forms_of_discipline_id'];
                $discipline->decision_agency_id                 = $validatedData['decision_agency_id'];
                $discipline->decision_id                        = $validatedData['decision_id'];
                $discipline->discipline_date                    = $validatedData['discipline_date'];

                $discipline->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($discipline);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        discipline::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $discipline = discipline::query()
            ->select([
                'id',
                'code_number',
                'school_id',
                'unit_id',
                'team_id',
                'forms_of_discipline_id',
                'decision_agency_id',
                'decision_id',
                'discipline_date',
            ])
            ->with('staffs',
                'schools',
                'units',
                'teams',
                'forms_of_disciplines',
                'decision_agencies',
                'decisions')
            ->whereHas('staffs', function($staffs) use($search) {
                $staffs->where('full_name', 'LIKE', "%$search%");
            })->orWhereHas('forms_of_disciplines', function($forms_of_disciplines) use($search) {
                $forms_of_disciplines->where('name', 'LIKE', "%$search%");
            })->orWhereHas('decision_agencies', function($decision_agencies) use($search) {
                $decision_agencies->where('name', 'LIKE', "%$search%");
            })
            ->orWhere('decision_id', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($discipline);
    }

    public function searchOptions(Request $request)
    {
        $searchAgency = $request->keyOptionDecisionAgency;
        $searchForms = $request->keyOptionFormsOfDiscipline;
        $start= $request->sign_date;
        $end = $request->received_date;

        if($searchAgency && !$searchForms && !$start && !$end) {
            $discipline = discipline::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_discipline_id',
                    'decision_agency_id',
                    'decision_id',
                    'discipline_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_disciplines',
                    'decision_agencies',
                    'decisions')
                ->where('decision_agency_id', '=', $searchAgency)
                ->DataTablePaginate($request);
            return $this->item($discipline);
        }
        else if($searchForms && !$searchAgency && !$start && !$end) {
            $discipline = discipline::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_discipline_id',
                    'decision_agency_id',
                    'decision_id',
                    'discipline_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_disciplines',
                    'decision_agencies',
                    'decisions')
                ->where('forms_of_discipline_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($discipline);
        }
        else if($start && $end && !$searchForms && !$searchAgency) {
            $discipline = discipline::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_discipline_id',
                    'decision_agency_id',
                    'decision_id',
                    'discipline_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_disciplines',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->DataTablePaginate($request);
            return $this->item($discipline);
        }
        else if($searchForms && $searchAgency && !$start && !$end) {
            $discipline = discipline::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_discipline_id',
                    'decision_agency_id',
                    'decision_id',
                    'discipline_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_disciplines',
                    'decision_agencies',
                    'decisions')
                ->where('decision_agency_id', '=', $searchAgency)
                ->where('forms_of_discipline_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($discipline);
        }
        else if ($searchForms && !$searchAgency && $start && $end) {
            $discipline = discipline::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_discipline_id',
                    'decision_agency_id',
                    'decision_id',
                    'discipline_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_disciplines',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->where('forms_of_discipline_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($discipline);
        }
        else if ($searchAgency && !$searchForms && $start && $end) {
            $discipline = discipline::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_discipline_id',
                    'decision_agency_id',
                    'decision_id',
                    'discipline_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_disciplines',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->where('decision_agency_id', '=', $searchAgency)
                ->DataTablePaginate($request);
            return $this->item($discipline);
        }
        else if ($searchForms && $searchAgency && $start && $end) {
            $discipline = discipline::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_discipline_id',
                    'decision_agency_id',
                    'decision_id',
                    'discipline_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_disciplines',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->where('decision_agency_id', '=', $searchAgency)
                ->where('forms_of_discipline_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($discipline);
        }
        else {
            $discipline = discipline::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_of_discipline_id',
                    'decision_agency_id',
                    'decision_id',
                    'discipline_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_of_disciplines',
                    'decision_agencies',
                    'decisions')
                ->DataTablePaginate($request);
            return $this->item($discipline);
        }
    }

    public function import()
    {
        Excel::import(new DisciplinesImport, request()->file('file'));

        return redirect('/')->with('success', 'All good!');
    }

    public function export()
    {
        return Excel::download(new DisciplinesExport, 'discipline.xlsx');
    }
}
