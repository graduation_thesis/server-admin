<?php

namespace App\Http\Controllers;

use App\collective_title;
use App\Http\Requests\CollectiveTitleCreateRequest;
use Illuminate\Support\Facades\DB;

class CollectiveTitleController extends AbstractApiController
{
    //
    public function index()
    {
        return response()->json(collective_title::get(), 200);
    }

    public function create(CollectiveTitleCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['collective_title_id']             = $validatedData['collective_title_id'];
        $payload['name']                            = $validatedData['name'];

        // Kiểm tra trùng tên
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại danh hiệu');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu đơn vị trên cơ sở
        $collective_title = collective_title::create($payload);
        DB::beginTransaction();

        try {
            $collective_title->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm danh hiệu thành công!');
            $this->setStatusCode(200);
            $this->setData($collective_title);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($collective_title_id)
    {
        return collective_title::findOrFail($collective_title_id);
    }

    public function update(CollectiveTitleCreateRequest $request, $collective_title_id)
    {
        $validatedData = $request->validated();

        $collective_title = collective_title::query()->findOrFail($collective_title_id);
        if (! $collective_title) {
            $this->setMessage('Không có danh hiệu này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $collective_title->collective_title_id          = $validatedData['collective_title_id'];
                $collective_title->name                         = $validatedData['name'];

                $collective_title->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($collective_title);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        collective_title::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $collective_title = collective_title::query()->get();
        foreach ($collective_title->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }
}
