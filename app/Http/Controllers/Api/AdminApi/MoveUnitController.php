<?php

namespace App\Http\Controllers;

use App\move_unit;
use App\staff;
use Illuminate\Http\Request;

class MoveUnitController extends Controller
{
    //
    public function index()
    {
        return response()->json(move_unit::with('staffs', 'units')->get(), 200);
    }

    public function create(Request $request)
    {
        $request->validate([
            'date_of_transfer' => 'required'
        ]);

        $staff = staff::find($request->staff_id);
        $staff->unit_id = $request->unit_id;


        $move_unit = new move_unit();
        $move_unit->staff_id = $request->staff_id;
        $move_unit->unit_id = $request->unit_id;
        $move_unit->date_of_transfer = $request->date_of_transfer;

        $result = $move_unit->save();
        $staff->save();

        if($result == 1)
        {
            return "Thành công!";
        }
        else
        {
            return "Thất bại";
        }

//        return $this->respond();
    }

    public function remove($id)
    {
        move_unit::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }
}
