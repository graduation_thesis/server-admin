<?php

namespace App\Http\Controllers;

use App\Http\Requests\RiskDutyCreateRequest;
use App\risk_duty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RiskDutyController extends AbstractApiController
{
    public function index(Request $request)
    {
        $risk_duty = risk_duty::query()
            ->select([
                'id',
                'code_number',
                'school_id',
                'unit_id',
                'team_id',
                'forms_risk_id',
                'decision_agency_id',
                'decision_id',
                'risk_date',
            ])
            ->with('staffs',
                'schools',
                'units',
                'teams',
                'forms_risks',
                'decision_agencies',
                'decisions')
            ->DataTablePaginate($request);

        return $this->item($risk_duty);
    }

    public function chart()
    {
        $risk_duty = risk_duty::all();

        $grouped = $risk_duty->groupBy(function ($item, $key) {
            return substr($item['risk_date'], 0, 4);
        });

        $groupCount = $grouped->map(function ($item, $key) {
            return collect($item)->count();
        });

        return $this->item($groupCount);
    }

    public function create(RiskDutyCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        foreach ($request['code_number'] as $item) {
            $payload['code_number']                     = $item['value'];
            $payload['school_id']                       = $item['valueSchoolId'];
            $payload['unit_id']                         = $item['valueUnitId'];
            $payload['team_id']                         = $item['valueTeamId'];
            $payload['forms_risk_id']                   = $validatedData['forms_risk_id'];
            $payload['decision_agency_id']              = $validatedData['decision_agency_id'];
            $payload['decision_id']                     = $validatedData['decision_id'];
            $payload['risk_date']                       = $validatedData['risk_date'];

            // Tạo và lưu thành tích cá nhân
            $risk_duty = risk_duty::create($payload);
            DB::beginTransaction();

            try {
                $risk_duty->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm rủi ro làm nhiệm vụ thành công!');
                $this->setStatusCode(200);
                $this->setData($risk_duty);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function show($id)
    {
        return risk_duty::with('staffs',
            'schools',
            'units',
            'teams',
            'forms_risks',
            'decision_agencies',
            'decisions')->findOrFail($id);
    }

    public function update(RiskDutyCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $risk_duty = risk_duty::query()->findOrFail($id);
        if (! $risk_duty) {
            $this->setMessage('Không có việc rủi ro cán bộ này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật rủi ro cán bộ
                $risk_duty->code_number                        = $validatedData['code_number'];
                $risk_duty->forms_risk_id                      = $validatedData['forms_risk_id'];
                $risk_duty->decision_agency_id                 = $validatedData['decision_agency_id'];
                $risk_duty->decision_id                        = $validatedData['decision_id'];
                $risk_duty->risk_date                          = $validatedData['risk_date'];

                $risk_duty->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($risk_duty);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        risk_duty::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $risk_duty = risk_duty::query()
            ->select([
                'id',
                'code_number',
                'school_id',
                'unit_id',
                'team_id',
                'forms_risk_id',
                'decision_agency_id',
                'decision_id',
                'risk_date',
            ])
            ->with('staffs',
                'schools',
                'units',
                'teams',
                'forms_risks',
                'decision_agencies',
                'decisions')
            ->whereHas('staffs', function($staffs) use($search) {
                $staffs->where('full_name', 'LIKE', "%$search%");
            })
            ->orWhereHas('forms_risks', function($forms_risks) use($search) {
                $forms_risks->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('decision_agencies', function($decision_agencies) use($search) {
                $decision_agencies->where('name', 'LIKE', "%$search%");
            })
            ->where('decision_id', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($risk_duty);
    }

    public function searchOptions(Request $request)
    {
        $searchForms = $request->keyOptionFormsRisk;
        $start= $request->sign_date;
        $end = $request->received_date;

         if($searchForms && !$start && !$end) {
            $risk_duty = risk_duty::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_risk_id',
                    'decision_agency_id',
                    'decision_id',
                    'risk_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_risks',
                    'decision_agencies',
                    'decisions')
                ->where('forms_risk_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($risk_duty);
        }
        else if($start && $end && !$searchForms) {
            $risk_duty = risk_duty::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_risk_id',
                    'decision_agency_id',
                    'decision_id',
                    'risk_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_risks',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->DataTablePaginate($request);
            return $this->item($risk_duty);
        } else if ($searchForms && $start && $end) {
            $risk_duty = risk_duty::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_risk_id',
                    'decision_agency_id',
                    'decision_id',
                    'risk_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_risks',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->where('forms_risk_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($risk_duty);
        }
        else {
            $risk_duty = risk_duty::query()
                ->select([
                    'id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'team_id',
                    'forms_risk_id',
                    'decision_agency_id',
                    'decision_id',
                    'risk_date',
                ])
                ->with('staffs',
                    'schools',
                    'units',
                    'teams',
                    'forms_risks',
                    'decision_agencies',
                    'decisions')
                ->DataTablePaginate($request);
            return $this->item($risk_duty);
        }
    }
}
