<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class UserController extends AbstractApiController
{
    public function index()
    {
        return response()->json(User::with('staffs')->get(), 200);
    }

    public function getPaginate(Request $request)
    {
        $User = User::query()
            ->select([
                'id',
                'code_number',
                'school_id',
                'unit_id',
                'username',
                'password',
                'role',
                'active'
            ])
            ->with('schools', 'units', 'staffs')
            ->DataTablePaginate($request);

        return $this->item($User);
    }

    public function isActive()
    {
        foreach (User::all() as $us) {
         if($us->isOnline()) {
             return response()->json('ok nha', 200);
         }
            return response()->json('ko nha', 200);
        }
    }

    public function create(UserCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['code_number']         = $validatedData['code_number'];
        $payload['school_id']           = $validatedData['school_id'];
        $payload['unit_id']             = $validatedData['unit_id'];
        $payload['username']            = $validatedData['username'];
        $payload['password']            = bcrypt($validatedData['password']);
        $payload['role']                = $validatedData['role'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['username'])) {
            $this->setMessage('Đã tồn tại tên tài khoản');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu tài khoản
        $User = User::create($payload);
        DB::beginTransaction();

        try {
            $User->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm tài khoản thành công!');
            $this->setStatusCode(200);
            $this->setData($User);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return User::findOrFail($id);
    }

    public function update(UserCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $User = User::query()->findOrFail($id);
        if (! $User) {
            $this->setMessage('Không có tài khoản này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên tài khoản
                $User->code_number                  = $validatedData['code_number'];
                $User->school_id                    = $validatedData['school_id'];
                $User->unit_id                      = $validatedData['unit_id'];
                $User->username                     = $validatedData['username'];
                $User->password                     = $validatedData['password'];
                $User->role                         = $validatedData['role'];

                $User->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($User);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        User::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($username)
    {
        $User = User::query()->get();
        foreach ($User->pluck('username') as $item) {
            if ($username == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $User = User::query()
            ->select([
                'id',
                'code_number',
                'school_id',
                'unit_id',
                'username',
                'password',
                'role',
                'active'
            ])
            ->with('schools', 'units', 'staffs')
            ->whereHas('schools', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('units', function($forms_of_rewards) use($search) {
                $forms_of_rewards->where('name', 'LIKE', "%$search%");
            })
            ->orWhere('username', 'LIKE', "%$search%")
            ->orWhere('code_number', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($User);
    }

    public function checkActive($id, $active)
    {
        $User = User::query()->findOrFail($id);
        if (! $User) {
            $this->setMessage('Không có tài khoản này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật trạng thái tài khoản
                $User->active                       = $active;

                $User->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật trạng thái tài khoản thành công');
                $this->setStatusCode(200);
                $this->setData($User);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();



//        foreach (User::all() as $us) {
//            if($us->isOnline()) {
//                return response()->json('ok nha', 200);
//            }
//            return response()->json('ko nha', 200);
//        }
    }
}
