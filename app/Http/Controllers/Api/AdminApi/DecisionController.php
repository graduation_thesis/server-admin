<?php

namespace App\Http\Controllers;

use App\decision;
use App\Http\Requests\DecisionCreateRequest;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DecisionController extends AbstractApiController
{
    public function index()
    {
        return response()->json(decision::get(), 200);
    }

    public function getPaginate(Request $request)
    {
        $decision = decision::query()
            ->select([
                'id',
                'decision_id',
                'decision_file',
                'sign_date',
                'received_date',
                'description',
            ])
            ->DataTablePaginate($request);

        return $this->item($decision);
    }

    public function create(DecisionCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['decision_id']             = $validatedData['decision_id'];
        $payload['decision_file']           = $validatedData['decision_file'];
        $payload['sign_date']               = $validatedData['sign_date'];
        $payload['received_date']           = $validatedData['received_date'];
        $payload['description']             = $validatedData['description'];


        // Kiểm tra trùng số quyết định
        if (! $this->checkDuplicateName($payload['decision_id'])) {
            $this->setMessage('Đã tồn tại số quyết định');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu số quyết định
        $decision = decision::create($payload);
        DB::beginTransaction();

        try {
            $decision->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm số quyết định thành công!');
            $this->setStatusCode(200);
            $this->setData($decision);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($decision_id)
    {
        return decision::findOrFail($decision_id);
    }

    public function update(DecisionCreateRequest $request, $decision_id)
    {
        $validatedData = $request->validated();

        $decision = decision::query()->findOrFail($decision_id);
        if (! $decision) {
            $this->setMessage('Không có quyết định này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật quyết định
                $decision->decision_id                  = $validatedData['decision_id'];
                $decision->decision_file                = $validatedData['decision_file'];
                $decision->sign_date                    = $validatedData['sign_date'];
                $decision->received_date                = $validatedData['received_date'];
                $decision->description                  = $validatedData['description'];

                $decision->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($decision);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        decision::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($decision_id)
    {
        $decision = decision::query()->get();
        foreach ($decision->pluck('decision_id') as $item) {
            if ($decision_id == $item) {
                return false;
            }
        }
        return true;
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            $Carbon = new Carbon();
            $theTime = Carbon::now()->format('Y-m-d');
            $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
            $request->image->move(public_path('file/decision'), $theImageName);
            return response()->json(['success' => 'có file'], 200);
        } else {
            return response()->json(['success' => 'chưa có file'], 200);
        }
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $decision = decision::query()
            ->select([
                'id',
                'decision_id',
                'decision_file',
                'sign_date',
                'received_date',
                'description',
            ])
            ->where('decision_id', 'LIKE', "%$search%")
            ->orWhere('decision_file', 'LIKE', "%$search%")
            ->orWhere('sign_date', 'LIKE', "%$search%")
            ->orWhere('received_date', 'LIKE', "%$search%")
            ->orWhere('description', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($decision);
    }

    public function searchOptions(Request $request)
    {
        $start= $request->sign_date;
        $end = $request->received_date;

        if($start && $end) {
            $decision = decision::query()
                ->select([
                    'id',
                    'decision_id',
                    'decision_file',
                    'sign_date',
                    'received_date',
                    'description',
                ])
                ->whereBetween('received_date', [$start, $end])
                ->DataTablePaginate($request);
            return $this->item($decision);
        } else {
            $decision = decision::query()
                ->select([
                    'id',
                    'decision_id',
                    'decision_file',
                    'sign_date',
                    'received_date',
                    'description',
                ])
                ->DataTablePaginate($request);
            return $this->item($decision);
        }
    }
}
