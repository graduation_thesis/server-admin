<?php

namespace App\Http\Controllers;

use App\Http\Requests\UnitCreateRequest;
use App\unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UnitController extends AbstractApiController
{
    //
    public function index()
    {
        $unit = unit::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'name',
                'build_date'
            ])
            ->with('schools')
            ->get();

        return $this->item($unit);
    }

    public function getPaginate(Request $request)
    {
        $unit = unit::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'name',
                'build_date'
            ])
            ->with('schools')
            ->DataTablePaginate($request);

        return $this->item($unit);
    }

    public function getUnits($id)
    {
        $unit = unit::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'name',
                'build_date'
            ])
            ->where('school_id', '=', $id)
            ->get();

        return $this->item($unit);
    }

    public function bundle($id)
    {
        $query = unit::query();
        $query->select([
            'id',
            'school_id',
            'unit_id',
            'name',
            'build_date'
        ]);
        $query->where('school_id', '=', $id);

        $unit = $query->firstOrFail();

        return $this->item($unit);
    }

    public function create(UnitCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['school_id'] = $validatedData['school_id'];
        $payload['unit_id'] = $validatedData['unit_id'];
        $payload['name'] = $validatedData['name'];
        $payload['build_date'] = $validatedData['build_date'];

        // Kiểm tra trùng tên
        if (! $this->checkDuplicateUnitID($payload['unit_id'])) {
            $this->setMessage('Đã tồn tại mã đơn vị trên cơ sở');
            $this->setStatusCode(400);
            return $this->respond();
        }

        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên đơn vị cơ sở');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu đơn vị trên cơ sở
        $unit = unit::create($payload);
        DB::beginTransaction();

        try {
            $unit->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm đơn vị cơ sở thành công!');
            $this->setStatusCode(200);
            $this->setData($unit);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($unit_id)
    {
        return unit::findOrFail($unit_id);
    }

    public function update(UnitCreateRequest $request, $unit_id)
    {
        $validatedData = $request->validated();

        $unit = unit::query()->findOrFail($unit_id);
        if (!$unit) {
            $this->setMessage('Không có đơn vị cơ sở này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $unit->school_id = $validatedData['school_id'];
                $unit->unit_id = $validatedData['unit_id'];
                $unit->name = $validatedData['name'];
                $unit->build_date = $validatedData['build_date'];

                $unit->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($unit);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        $staff = unit::query()->select()->where('unit_id', '=', $id)->with('staffs')->get();

        if($staff) {
            foreach ($staff as $item) {
                $item->delete();
            }
        }
        $unit = unit::query()->where('unit_id', '=', $id);
        $unit->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    private function checkDuplicateName($name)
    {
        $unit = unit::query()->get();
        foreach ($unit->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    private function checkDuplicateUnitID($unit_id)
    {
        $unit = unit::query()->get();
        foreach ($unit->pluck('unit_id') as $item) {
            if ($unit_id == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $unit = unit::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'name',
                'build_date'
            ])
            ->with('schools')
            ->whereHas('schools', function($schools) use($search) {
                $schools->where('name', 'LIKE', "%$search%");
            })
            ->orWhere('name', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($unit);
    }
}
