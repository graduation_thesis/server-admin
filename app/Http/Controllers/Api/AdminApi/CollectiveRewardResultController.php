<?php

namespace App\Http\Controllers;

use App\collective_reward_result;
use App\Exports\CheckedRewardExport;
use App\Http\Requests\CollectiveRewardResultCreateRequest;
use App\Imports\CheckedRewardsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class CollectiveRewardResultController extends AbstractApiController
{
    public function index(Request $request)
    {
        $collective_reward_result = collective_reward_result::query()
            ->select([
                'id',
                'user_id',
                'school_id',
                'unit_id',
                'collective_title_id',
                'reward_date',
            ])
            ->with('users',
                'schools',
                'units',
                'collective_titles')
            ->DataTablePaginate($request);

        return $this->item($collective_reward_result);
    }


    public function create(CollectiveRewardResultCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        foreach ($request['unit_id'] as $item) {
            $payload['user_id']                         = $validatedData['user_id'];
            $payload['school_id']                       = $item['valueSchoolId'];
            $payload['unit_id']                         = $item['value'];
            $payload['collective_title_id']             = $validatedData['collective_title_id'];
            $payload['approved_by']                     = $validatedData['user_id'];
            $payload['reward_date']                     = $validatedData['reward_date'];

            // Tạo và lưu thành tích cá nhân
            $collective_reward_result = collective_reward_result::create($payload);
            DB::beginTransaction();

            try {
                $collective_reward_result->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm kết quả thi đua thành công!');
                $this->setStatusCode(200);
                $this->setData($collective_reward_result);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function show($id)
    {
        return collective_reward_result::with('users',
            'schools',
            'units',
            'collective_titles')->findOrFail($id);
    }

    public function update(CollectiveRewardResultCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $collective_reward_result = collective_reward_result::query()->findOrFail($id);
        if (! $collective_reward_result) {
            $this->setMessage('Không có kết quả thi đua này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật khen thưởng đơn vị
                $collective_reward_result->user_id                             = $validatedData['user_id'];
                $collective_reward_result->school_id                           = $validatedData['school_id'];
                $collective_reward_result->unit_id                             = $validatedData['unit_id'];
                $collective_reward_result->collective_title_id                 = $validatedData['collective_title_id'];
                $collective_reward_result->approved_by                         = $validatedData['approved_by'];
                $collective_reward_result->reward_date                         = $validatedData['reward_date'];

                $collective_reward_result->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($collective_reward_result);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }


    public function remove($id)
    {
        collective_reward_result::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $collective_reward_result = collective_reward_result::query()
            ->select([
                'id',
                'user_id',
                'school_id',
                'unit_id',
                'collective_title_id',
                'reward_date',
            ])
            ->with('users',
                'schools',
                'units',
                'collective_titles')
            ->whereHas('users', function($users) use($search) {
                $users->where('username', 'LIKE', "%$search%");
            })
            ->orWhereHas('schools', function($schools) use($search) {
                $schools->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('units', function($units) use($search) {
                $units->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('collective_titles', function($collective_titles) use($search) {
                $collective_titles->where('name', 'LIKE', "%$search%");
            })
            ->DataTablePaginate($request);
        return $this->item($collective_reward_result);
    }

    public function searchOptions(Request $request)
    {
        $searchTitle = $request->keyOptionPersonalTitle;

        if($searchTitle) {
            $collective_reward_result = collective_reward_result::query()
                ->select([
                    'id',
                    'user_id',
                    'school_id',
                    'unit_id',
                    'collective_title_id',
                    'reward_date',
                ])
                ->with('users',
                    'schools',
                    'units',
                    'collective_titles')
                ->where('collective_title_id', '=', $searchTitle)
                ->DataTablePaginate($request);
            return $this->item($collective_reward_result);
        } else {
            $collective_reward_result = collective_reward_result::query()
                ->select([
                    'id',
                    'user_id',
                    'school_id',
                    'unit_id',
                    'collective_title_id',
                    'reward_date',
                ])
                ->with('users',
                    'schools',
                    'units',
                    'collective_titles')
                ->DataTablePaginate($request);
            return $this->item($collective_reward_result);
        }
    }

    public function import()
    {
        Excel::import(new CheckedRewardsImport, request()->file('file'));

        return redirect('/')->with('success', 'All good!');
    }

    public function export()
    {
        return Excel::download(new CheckedRewardExport, 'invoices.xlsx');
    }
}
