<?php

namespace App\Http\Controllers;

use App\Exports\InvoicesExport;
use App\Http\Requests\StaffCreateRequest;
use App\staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\StaffImport;
use App\Exports\StaffExport;

class StaffController extends AbstractApiController
{
    public function index()
    {
        $staff = staff::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'team_id',
                'full_name',
                'birthday',
                'sex',
                'code_number',
                'type_staff_id',
                'rank_staff_id',
                'position_staff_id',
                'created_at'
            ])
            ->with('schools','units','teams','ranks','positions','types')
            ->get();

        return $this->item($staff);
    }

    public function getPaginate(Request $request)
    {
        $staff = staff::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'team_id',
                'full_name',
                'birthday',
                'sex',
                'code_number',
                'type_staff_id',
                'rank_staff_id',
                'position_staff_id',
                'created_at'
            ])
            ->with('schools','units','teams','ranks','positions','types')
            ->DataTablePaginate($request);
        return $this->item($staff);
    }

    public function getStaffs($id)
    {
        $staff = staff::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'team_id',
                'full_name',
                'birthday',
                'sex',
                'code_number',
                'type_staff_id',
                'rank_staff_id',
                'position_staff_id',
                'created_at'
            ])
            ->with('schools','units','teams','ranks','positions','types')
            ->where('unit_id', '=', $id)
            ->get();

        return $this->item($staff);
    }

    public function bundle($id)
    {
        $query = staff::query();
        $query->select([
            'id',
            'school_id',
            'unit_id',
            'team_id',
            'full_name',
            'birthday',
            'sex',
            'code_number',
            'type_staff_id',
            'rank_staff_id',
            'position_staff_id',
            'created_at'
        ]);
        $query->where('unit_id', '=', $id);

        $staff = $query->firstOrFail();

        return $this->item($staff);
    }

    public function create(StaffCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['school_id']                       = $validatedData['school_id'];
        $payload['unit_id']                         = $validatedData['unit_id'];
        $payload['team_id']                         = $validatedData['team_id'];
        $payload['full_name']                       = $validatedData['full_name'];
        $payload['birthday']                        = $validatedData['birthday'];
        $payload['code_number']                     = $validatedData['code_number'];
        $payload['sex']                             = $validatedData['sex'];
        $payload['rank_staff_id']                   = ! empty($validatedData['rank_staff_id']) ? $validatedData['rank_staff_id'] : null;
        $payload['position_staff_id']               = ! empty($validatedData['position_staff_id']) ? $validatedData['position_staff_id'] : null;
        $payload['type_staff_id']                   = ! empty($validatedData['type_staff_id']) ? $validatedData['type_staff_id'] : null;

        // Kiểm tra trùng tên số hiệu
        if (! $this->checkDuplicateName($payload['code_number'])) {
            $this->setMessage('Đã tồn tại số hiệu cán bộ');
            $this->setStatusCode(400);
            return $this->respond();
        }
        // Tạo và lưu cán bộ
        $staff = staff::create($payload);
        DB::beginTransaction();

        try {
            $staff->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm cán bộ thành công!');
            $this->setStatusCode(200);
            $this->setData($staff);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return staff::with('schools','units','teams','ranks','positions','types')->findOrFail($id);
    }

    public function update(StaffCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $staff = staff::query()->findOrFail($id);
        if (! $staff) {
            $this->setMessage('Không có cán bộ này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật cán bộ
                $staff->school_id                       = $validatedData['school_id'];
                $staff->unit_id                         = $validatedData['unit_id'];
                $staff->team_id                         = $validatedData['team_id'];
                $staff->full_name                       = $validatedData['full_name'];
                $staff->birthday                        = $validatedData['birthday'];
                $staff->sex                             = $validatedData['sex'];
                $staff->code_number                     = $validatedData['code_number'];
                if($validatedData['type_staff_id'] == 'null') {
                    $staff->type_staff_id               = null;

                } else {
                    $staff->type_staff_id               = $validatedData['type_staff_id'];
                }
                if($validatedData['rank_staff_id'] == 'null') {
                    $staff->rank_staff_id               = null;

                } else {
                    $staff->rank_staff_id               = $validatedData['rank_staff_id'];
                }
                if($validatedData['position_staff_id'] == 'null') {
                   $staff->position_staff_id               = null;

                } else {
                    $staff->position_staff_id               = $validatedData['position_staff_id'];
                }

                $staff->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($staff);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        staff::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($code_number)
    {
        $staff = staff::query()->get();
        foreach ($staff->pluck('code_number') as $item) {
            if ($code_number == $item) {
                return false;
            }
        }
        return true;
    }

    public function import()
    {
        Excel::import(new StaffImport, request()->file('file'));

        return redirect('/')->with('success', 'All good!');
    }

    public function export()
    {
        return Excel::download(new StaffExport, 'staff.xlsx');
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $staff = staff::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'team_id',
                'full_name',
                'birthday',
                'sex',
                'code_number',
                'type_staff_id',
                'rank_staff_id',
                'position_staff_id',
                'created_at'
            ])
            ->with('schools','units','teams','ranks','positions','types')
            ->whereHas('schools', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('units', function($forms_of_rewards) use($search) {
                $forms_of_rewards->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('teams', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('ranks', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('positions', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('types', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhere('full_name', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($staff);
    }

    public function profile($id)
    {
        $staff = staff::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'team_id',
                'full_name',
                'birthday',
                'sex',
                'code_number',
                'type_staff_id',
                'rank_staff_id',
                'position_staff_id',
                'created_at'
            ])
            ->with('schools','units','teams','ranks','positions','types')
            ->where('code_number', '=', $id)
            ->get();

        return $this->item($staff);
    }

    public function download($id)
    {
        return (new InvoicesExport($id))->download('Ho_So_Ca_Nhan.xlsx');
    }
}
