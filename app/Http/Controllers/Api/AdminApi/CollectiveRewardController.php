<?php

namespace App\Http\Controllers;

use App\collective_reward;
use App\Http\Requests\CollectiveRewardCreateRequest;
use App\Imports\CollectiveRewardsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class CollectiveRewardController extends AbstractApiController
{
    public function index(Request $request)
    {
        $collective_reward = collective_reward::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'forms_of_reward_id',
                'decision_agency_id',
                'decision_id',
                'reward_date',
            ])
            ->with('schools',
                'units',
                'forms_of_rewards',
                'decision_agencies',
                'decisions')
            ->DataTablePaginate($request);

        return $this->item($collective_reward);
    }

    public function chart()
    {
        $collective_reward = collective_reward::all();

        $grouped = $collective_reward->groupBy(function ($item, $key) {
            return substr($item['reward_date'], 0, 4);
        });

        $groupCount = $grouped->map(function ($item, $key) {
            return collect($item)->count();
        });

        return $this->item($groupCount);
    }

    public function create(CollectiveRewardCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        foreach ($request['unit_id'] as $item) {
            $payload['unit_id']                     = $item['value'];
            $payload['school_id']                   = $item['valueSchoolId'];

            if (($validatedData['forms_of_reward_id']) !== "null") {
                $payload['forms_of_reward_id']      = $validatedData['forms_of_reward_id'];
            }
            $payload['decision_agency_id']          = $validatedData['decision_agency_id'];
            $payload['decision_id']                 = $validatedData['decision_id'];
            $payload['reward_date']                 = $validatedData['reward_date'];

            // Tạo và lưu thành tích cá nhân
            $collective_reward = collective_reward::create($payload);
            DB::beginTransaction();

            try {
                $collective_reward->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm thành tích tập thể thành công!');
                $this->setStatusCode(200);
                $this->setData($collective_reward);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function show($id)
    {
        return collective_reward::with('schools',
            'units',
            'forms_of_rewards',
            'decision_agencies',
            'decisions')->findOrFail($id);
    }

    public function update(CollectiveRewardCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $collective_reward = collective_reward::query()->findOrFail($id);
        if (! $collective_reward) {
            $this->setMessage('Không có việc khen thưởng đơn vị này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật khen thưởng đơn vị
                $collective_reward->unit_id                             = $validatedData['unit_id'];

                if (($validatedData['forms_of_reward_id']) !== "null") {
                    $collective_reward->forms_of_reward_id              = $validatedData['forms_of_reward_id'];
                } else {
                    $collective_reward->forms_of_reward_id              = null;
                }
                $collective_reward->decision_agency_id                  = $validatedData['decision_agency_id'];
                $collective_reward->decision_id                         = $validatedData['decision_id'];
                $collective_reward->reward_date                         = $validatedData['reward_date'];

                $collective_reward->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($collective_reward);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        collective_reward::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $collective_reward = collective_reward::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'forms_of_reward_id',
                'decision_agency_id',
                'decision_id',
                'reward_date',
            ])
            ->with('schools',
                'units',
                'forms_of_rewards',
                'decision_agencies',
                'decisions')
            ->whereHas('units', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })->orWhereHas('forms_of_rewards', function($forms_of_rewards) use($search) {
                $forms_of_rewards->where('name', 'LIKE', "%$search%");
            })
            ->DataTablePaginate($request);
        return $this->item($collective_reward);
    }

    public function searchOptions(Request $request)
    {
        $searchForms = $request->keyOptionFormsOfReward;
        $start= $request->sign_date;
        $end = $request->received_date;

        if($searchForms && !$start && !$end) {
            $collective_reward = collective_reward::query()
                ->select([
                    'id',
                    'school_id',
                    'unit_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('schools',
                    'units',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->where('forms_of_reward_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($collective_reward);
        }
        else if($start && $end && !$searchForms) {
            $collective_reward = collective_reward::query()
                ->select([
                    'id',
                    'school_id',
                    'unit_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('schools',
                    'units',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->DataTablePaginate($request);
            return $this->item($collective_reward);
        }
        else if($searchForms && !$start && !$end) {
            $collective_reward = collective_reward::query()
                ->select([
                    'id',
                    'school_id',
                    'unit_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('schools',
                    'units',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->where('forms_of_reward_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($collective_reward);
        }
        else if ($searchForms && $start && $end) {
            $collective_reward = collective_reward::query()
                ->select([
                    'id',
                    'school_id',
                    'unit_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('schools',
                    'units',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->where('forms_of_reward_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($collective_reward);
        }
        else if (!$searchForms && $start && $end) {
            $collective_reward = collective_reward::query()
                ->select([
                    'id',
                    'school_id',
                    'unit_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('schools',
                    'units',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->DataTablePaginate($request);
            return $this->item($collective_reward);
        }
        else if ($searchForms && $start && $end) {
            $collective_reward = collective_reward::query()
                ->select([
                    'id',
                    'school_id',
                    'unit_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('schools',
                    'units',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->whereHas('decisions', function($decisions) use($start, $end) {
                    $decisions->whereBetween('received_date', [$start, $end]);
                })
                ->where('forms_of_reward_id', '=', $searchForms)
                ->DataTablePaginate($request);
            return $this->item($collective_reward);
        }
        else {
            $collective_reward = collective_reward::query()
                ->select([
                    'id',
                    'school_id',
                    'unit_id',
                    'forms_of_reward_id',
                    'decision_agency_id',
                    'decision_id',
                    'reward_date',
                ])
                ->with('schools',
                    'units',
                    'forms_of_rewards',
                    'decision_agencies',
                    'decisions')
                ->DataTablePaginate($request);
            return $this->item($collective_reward);
        }
    }

    public function import()
    {
        Excel::import(new CollectiveRewardsImport, request()->file('file'));

        return redirect('/')->with('success', 'All good!');
    }
}
