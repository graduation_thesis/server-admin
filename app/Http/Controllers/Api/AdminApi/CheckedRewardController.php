<?php

namespace App\Http\Controllers;

use App\checked_reward;
use App\Http\Requests\CheckedRewardCreateRequest;
use App\Imports\CheckedRewardsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CheckedRewardExport;

class CheckedRewardController extends AbstractApiController
{
    public function index(Request $request)
    {
        $checked_reward = checked_reward::query()
            ->select([
                'id',
                'user_id',
                'school_id',
                'unit_id',
                'rank_staff_id',
                'code_number',
                'personal_title_id',
                'reward_date',
            ])
            ->with('users',
                'staffs',
                'schools',
                'units',
                'ranks',
                'personal_titles')
            ->DataTablePaginate($request);

        return $this->item($checked_reward);
    }


    public function create(CheckedRewardCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        foreach ($request['code_number'] as $item) {
            $payload['user_id']                     = $validatedData['user_id'];
            $payload['code_number']                 = $item['valueCodeNumber'];
            $payload['school_id']                   = $item['valueSchoolId'];
            $payload['unit_id']                     = $item['valueUnitId'];
            $payload['rank_staff_id']               = $item['valueRankId'];
            $payload['personal_title_id']           = $validatedData['personal_title_id'];
            $payload['approved_by']                 = $validatedData['user_id'];
            $payload['reward_date']                 = $validatedData['reward_date'];

            // Tạo và lưu thành tích cá nhân
            $checked_reward = checked_reward::create($payload);
            DB::beginTransaction();

            try {
                $checked_reward->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm đăng kí thi đua thành công!');
                $this->setStatusCode(200);
                $this->setData($checked_reward);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function show($id)
    {
        return checked_reward::with('users',
            'staffs',
            'ranks',
            'schools',
            'units',
            'personal_titles')->findOrFail($id);
    }

    public function update(CheckedRewardCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $checked_reward = checked_reward::query()->findOrFail($id);
        if (! $checked_reward) {
            $this->setMessage('Không có việc đăng kí thi đua này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật khen thưởng đơn vị
                $checked_reward->user_id                             = $validatedData['user_id'];
                $checked_reward->code_number                         = $validatedData['code_number'];
                $checked_reward->school_id                           = $validatedData['school_id'];
                $checked_reward->unit_id                             = $validatedData['unit_id'];
//                $checked_reward->rank_staff_id                       = $validatedData['rank_staff_id'];
                $checked_reward->rank_staff_id                       = ! empty($validatedData['rank_staff_id']) ? $validatedData['rank_staff_id'] : null;

                $checked_reward->personal_title_id                   = $validatedData['personal_title_id'];
                $checked_reward->approved_by                         = $validatedData['approved_by'];
                $checked_reward->reward_date                         = $validatedData['reward_date'];

                $checked_reward->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($checked_reward);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }


    public function remove($id)
    {
        checked_reward::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $checked_reward = checked_reward::query()
            ->select([
                'id',
                'user_id',
                'school_id',
                'unit_id',
                'rank_staff_id',
                'code_number',
                'personal_title_id',
                'reward_date',
            ])
            ->with('users',
                'staffs',
                'ranks',
                'schools',
                'units',
                'staffs',
                'personal_titles')
            ->whereHas('users', function($staffs) use($search) {
                $staffs->where('username', 'LIKE', "%$search%");
            })
            ->orWhereHas('schools', function($schools) use($search) {
                $schools->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('units', function($units) use($search) {
                $units->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('staffs', function($staffs) use($search) {
                $staffs->where('full_name', 'LIKE', "%$search%");
            })
            ->orWhereHas('ranks', function($ranks) use($search) {
                $ranks->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('personal_titles', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhere('code_number', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($checked_reward);
    }

    public function searchOptions(Request $request)
    {
        $searchTitle = $request->keyOptionPersonalTitle;

        if($searchTitle) {
            $checked_reward = checked_reward::query()
                ->select([
                    'id',
                    'user_id',
                    'school_id',
                    'unit_id',
                    'rank_staff_id',
                    'code_number',
                    'personal_title_id',
                    'reward_date',
                ])
                ->with('users',
                    'staffs',
                    'ranks',
                    'schools',
                    'units',
                    'personal_titles')
                ->where('personal_title_id', '=', $searchTitle)
                ->DataTablePaginate($request);
            return $this->item($checked_reward);
        } else {
            $checked_reward = checked_reward::query()
                ->select([
                    'id',
                    'user_id',
                    'school_id',
                    'unit_id',
                    'rank_staff_id',
                    'code_number',
                    'personal_title_id',
                    'reward_date',
                ])
                ->with('users',
                    'staffs',
                    'ranks',
                    'schools',
                    'units',
                    'personal_titles')
                ->DataTablePaginate($request);
            return $this->item($checked_reward);
        }
    }

    public function import()
    {
        Excel::import(new CheckedRewardsImport, request()->file('file'));

        return redirect('/')->with('success', 'All good!');
    }

    public function export()
    {
        return Excel::download(new CheckedRewardExport, 'invoices.xlsx');
    }
}
