<?php

namespace App\Http\Controllers;

use App\checked_collective_reward;
use App\Http\Requests\CheckedCollectiveRewardCreateRequest;
use App\register_collective_reward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegisterCollectiveRewardController extends AbstractApiController
{
    public function index(Request $request)
    {
        $register_collective_reward = register_collective_reward::query()
            ->select([
                'id',
                'user_id',
                'school_id',
                'unit_id',
                'collective_title_id',
                'reward_date',
            ])
            ->with('users',
                'schools',
                'units',
                'collective_titles')
            ->DataTablePaginate($request);

        return $this->item($register_collective_reward);
    }

    public function checked(CheckedCollectiveRewardCreateRequest $request, $id, $checker)
    {
        $validatedData = $request->validated();

//        $register_reward = register_reward::query()->findOrFail($id);
//        if (! $register_reward) {
//            $this->setMessage('Không có việc đăng kí thi đua này');
//            $this->setStatusCode(400);
//        } else {
        $payload = [];
        $payload['user_id']                     = $validatedData['user_id'];
        $payload['school_id']                   = $validatedData['school_id'];
        $payload['unit_id']                     = $validatedData['unit_id'];
        $payload['collective_title_id']         = $validatedData['collective_title_id'];
        $payload['approved_by']                 = $checker;
        $payload['reward_date']                 = $validatedData['reward_date'];

        // Tạo và lưu đăng kí đã thông qua kiểm duyệt
        $checked_collective_reward = checked_collective_reward::create($payload);
        DB::beginTransaction();
        try {
            $checked_collective_reward->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Kiểm duyệt thành công!');
            $this->setStatusCode(200);
            $this->setData($checked_collective_reward);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
//        }
        return $this->respond();
    }

    public function checkedAll(CheckedCollectiveRewardCreateRequest $request, $id, $checker)
    {
        $validatedData = $request->validated();

//        $register_reward = register_reward::query()->findOrFail($id);
//        if (! $register_reward) {
//            $this->setMessage('Không có việc đăng kí thi đua này');
//            $this->setStatusCode(400);
//        } else {
        $payload = [];
        foreach ($request['id'] as $item) {

            $payload['user_id']                         = $item['user_id'];
            $payload['school_id']                       = $item['school_id'];
            $payload['unit_id']                         = $item['unit_id'];
            $payload['collective_title_id']             = $item['collective_title_id'];
            $payload['reward_date']                     = $item['reward_date'];
            $payload['approved_by']                     = $checker;

            // Tạo và lưu đăng kí đã thông qua kiểm duyệt
            $checked_collective_reward = checked_collective_reward::create($payload);
            DB::beginTransaction();
            try {
                $checked_collective_reward->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Kiểm duyệt thành công!');
                $this->setStatusCode(200);
                $this->setData($checked_collective_reward);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        register_collective_reward::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $register_collective_reward = register_collective_reward::query()
            ->select([
                'id',
                'user_id',
                'school_id',
                'unit_id',
                'collective_title_id',
                'reward_date',
            ])
            ->with('users',
                'schools',
                'units',
                'collective_titles')
            ->whereHas('users', function ($users) use ($search) {
                $users->where('username', 'LIKE', "%$search%");
            })->orWhereHas('schools', function ($schools) use ($search) {
                $schools->where('name', 'LIKE', "%$search%");
            })->orWhereHas('units', function ($units) use ($search) {
                $units->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('collective_titles', function ($collective_titles) use ($search) {
                $collective_titles->where('name', 'LIKE', "%$search%");
            })
            ->DataTablePaginate($request);
        return $this->item($register_collective_reward);
    }

    public function searchOptions(Request $request)
    {
        $searchTitle = $request->keyOptionPersonalTitle;

        if ($searchTitle) {
            $register_collective_reward = register_collective_reward::query()
                ->select([
                    'id',
                    'user_id',
                    'school_id',
                    'unit_id',
                    'collective_title_id',
                    'reward_date',
                ])
                ->with('users',
                    'schools',
                    'units',
                    'collective_titles')
                ->where('collective_title_id', '=', $searchTitle)
                ->DataTablePaginate($request);
            return $this->item($register_collective_reward);
        } else {
            $register_collective_reward = register_collective_reward::query()
                ->select([
                    'id',
                    'user_id',
                    'school_id',
                    'unit_id',
                    'collective_title_id',
                    'reward_date',
                ])
                ->with('users',
                    'schools',
                    'units',
                    'collective_titles')
                ->DataTablePaginate($request);
            return $this->item($register_collective_reward);
        }
    }
}
