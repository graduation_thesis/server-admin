<?php

namespace App\Http\Controllers;

use App\change_collective;
use App\staff;
use App\unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChangeCollectiveController extends AbstractApiController
{
    public function index(Request $request)
    {
        $change_collective = change_collective::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'unit_id_changed',
                'created_at'
            ])
            ->with('schools', 'units', 'new_units')
            ->DataTablePaginate($request);
        return $this->item($change_collective);
    }


    public function create(Request $request)
    {
        $staff = staff::query()->where('unit_id', '=', $request['unit_id'])->get();

        $unit = unit::query()->where('unit_id', '=', $request['unit_id']);

        if (!$staff) {
            $this->setMessage('Không có đơn vị này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật cán bộ trong quá trình chuyển 1 đơn vị này sang đơn vị khác
                foreach ($staff as $item) {
                    $item->code_number                  = $item['code_number'];
                    $item->school_id                    = $item['school_id'];
                    $item->unit_id                      = $request['unit_id_changed'];
                    $item->team_id                      = $item['team_id'];
                    $item->full_name                    = $item['full_name'];
                    $item->birthday                     = $item['birthday'];
                    $item->sex                          = $item['sex'];
                    $item->type_staff_id                = $item['type_staff_id'];
                    $item->rank_staff_id                = $item['rank_staff_id'];
                    $item->position_staff_id            = $item['position_staff_id'];

                    $item->save();
                    DB::commit();

                    // Trả về kết quả
                    $this->setMessage('Cập nhật thành công');
                    $this->setStatusCode(200);
                    $this->setData($item);
                }

                 //Lưu lịch sử khi thay đổi đơn vị
                $change_collective = new change_collective();

                $change_collective->school_id                       = $request['school_id'];
                $change_collective->unit_id                         = $request['unit_id'];
                $change_collective->unit_id_changed                 = $request['unit_id_changed'];

                $change_collective->save();
                // End lưu thay đổi lịch sử

                // Xóa đơn vị sau khi đã chuyển
//                $unit->delete();


            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
