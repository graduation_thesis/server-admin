<?php

namespace App\Http\Controllers;

use App\change_personal;
use App\staff;
use App\unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChangePersonalController extends AbstractApiController
{
    public function index(Request $request)
    {
        $change_personal = change_personal::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'unit_id_changed',
                'team_id',
                'full_name',
                'birthday',
                'sex',
                'code_number',
                'type_staff_id',
                'rank_staff_id',
                'position_staff_id',
                'created_at'
            ])
            ->with('schools','units','teams','ranks','positions','types', 'new_units')
            ->DataTablePaginate($request);
        return $this->item($change_personal);
    }

    public function create(Request $request)
    {
        foreach ($request['code_number'] as $item) {
            $staff = staff::query()->findOrFail($item['id']);

            if (!$staff) {
                $this->setMessage('Không có cán bộ này');
                $this->setStatusCode(400);
            } else {
                DB::beginTransaction();

                try {
                    // Cập nhật cán bộ khi đã chuyển công tác
                    $staff->code_number             = $item['value'];
                    $staff->school_id               = $item['valueSchoolId'];
                    $staff->unit_id                 = $request['unit_id'];
                    $staff->team_id                 = $item['valueTeamId'];
                    $staff->full_name               = $item['txtFullName'];
                    $staff->birthday                = $item['txtBirthday'];
                    $staff->sex                     = $item['txtSex'];
                    $staff->type_staff_id           = $item['valueTypeId'];
                    $staff->rank_staff_id           = $item['valueRankId'];
                    $staff->position_staff_id       = $item['valuePositionId'];

                    $staff->save();
                    DB::commit();

                    // Lưu lại lịch sử của cán bộ trước khi chuyển
                    $change_personal = new change_personal();

                    $change_personal->code_number             = $item['value'];
                    $change_personal->school_id               = $item['valueSchoolId'];
                    $change_personal->unit_id                 = $item['valueUnitId'];
                    $change_personal->unit_id_changed         = $request['unit_id'];
                    $change_personal->team_id                 = $item['valueTeamId'];
                    $change_personal->full_name               = $item['txtFullName'];
                    $change_personal->birthday                = $item['txtBirthday'];
                    $change_personal->sex                     = $item['txtSex'];
                    $change_personal->type_staff_id           = $item['valueTypeId'];
                    $change_personal->rank_staff_id           = $item['valueRankId'];
                    $change_personal->position_staff_id       = $item['valuePositionId'];

                    $change_personal->save();


                    // Trả về kết quả
                    $this->setMessage('Cập nhật thành công');
                    $this->setStatusCode(200);
                    $this->setData($staff);
                } catch (Exception $e) {
                    report($e);
                    DB::rollBack();

                    // Thông báo lỗi
                    $this->setMessage($e->getMessage());
                    $this->setStatusCode(500);
                }
            }
        }
        return $this->respond();
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $change_personal = change_personal::query()
            ->select([
                'id',
                'school_id',
                'unit_id',
                'unit_id_changed',
                'team_id',
                'full_name',
                'birthday',
                'sex',
                'code_number',
                'type_staff_id',
                'rank_staff_id',
                'position_staff_id',
                'created_at'
            ])
            ->with('schools','units','teams','ranks','positions','types')
            ->whereHas('schools', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('units', function($forms_of_rewards) use($search) {
                $forms_of_rewards->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('teams', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('ranks', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('positions', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('types', function($staffs) use($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhere('full_name', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($change_personal);
    }

    public function remove($id)
    {
        change_personal::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }
}
