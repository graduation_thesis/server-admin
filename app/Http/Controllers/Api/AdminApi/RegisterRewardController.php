<?php

namespace App\Http\Controllers;

use App\checked_reward;
use App\Http\Requests\CheckedRewardCreateRequest;
use App\register_reward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegisterRewardController extends AbstractApiController
{
    public function index(Request $request)
    {
        $register_reward = register_reward::query()
            ->select([
                'id',
                'user_id',
                'code_number',
                'school_id',
                'unit_id',
                'rank_staff_id',
                'personal_title_id',
                'reward_date',
            ])
            ->with('users',
                'staffs',
                'schools',
                'units',
                'ranks',
                'personal_titles')
            ->DataTablePaginate($request);

        return $this->item($register_reward);
    }

    public function checked(CheckedRewardCreateRequest $request, $id, $checker)
    {
        $validatedData = $request->validated();

//        $register_reward = register_reward::query()->findOrFail($id);
//        if (! $register_reward) {
//            $this->setMessage('Không có việc đăng kí thi đua này');
//            $this->setStatusCode(400);
//        } else {
        $payload = [];
        $payload['user_id']                         = $validatedData['user_id'];
        $payload['code_number']                     = $validatedData['code_number'];
        $payload['school_id']                       = $validatedData['school_id'];
        $payload['unit_id']                         = $validatedData['unit_id'];
        $payload['rank_staff_id']                   = $validatedData['rank_staff_id'];
        $payload['personal_title_id']               = $validatedData['personal_title_id'];
        $payload['reward_date']                     = $validatedData['reward_date'];
        $payload['approved_by']                     = $checker;

        // Tạo và lưu đăng kí đã thông qua kiểm duyệt
        $checked_reward = checked_reward::create($payload);
        DB::beginTransaction();
        try {
            $checked_reward->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Kiểm duyệt thành công!');
            $this->setStatusCode(200);
            $this->setData($checked_reward);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
//        }
        return $this->respond();
    }

    public function checkedAll(CheckedRewardCreateRequest $request, $id, $checker)
    {
        $validatedData = $request->validated();

//        $register_reward = register_reward::query()->findOrFail($id);
//        if (! $register_reward) {
//            $this->setMessage('Không có việc đăng kí thi đua này');
//            $this->setStatusCode(400);
//        } else {
        $payload = [];
        foreach ($request['id'] as $item) {

            $payload['user_id']                         = $item['user_id'];
            $payload['code_number']                     = $item['code_number'];
            $payload['school_id']                       = $item['school_id'];
            $payload['unit_id']                         = $item['unit_id'];
            $payload['rank_staff_id']                   = $item['rank_staff_id'];
            $payload['personal_title_id']               = $item['personal_title_id'];
            $payload['reward_date']                     = $item['reward_date'];
            $payload['approved_by']                     = $checker;

            // Tạo và lưu đăng kí đã thông qua kiểm duyệt
            $checked_reward = checked_reward::create($payload);
            DB::beginTransaction();
            try {
                $checked_reward->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Kiểm duyệt thành công!');
                $this->setStatusCode(200);
                $this->setData($checked_reward);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        register_reward::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $register_reward = register_reward::query()
            ->select([
                'id',
                'user_id',
                'code_number',
                'school_id',
                'unit_id',
                'rank_staff_id',
                'personal_title_id',
                'reward_date',
            ])
            ->with('users',
                'staffs',
                'schools',
                'units',
                'ranks',
                'personal_titles')
            ->whereHas('users', function ($staffs) use ($search) {
                $staffs->where('username', 'LIKE', "%$search%");
            })->orWhereHas('schools', function ($schools) use ($search) {
                $schools->where('name', 'LIKE', "%$search%");
            })->orWhereHas('units', function ($units) use ($search) {
                $units->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('personal_titles', function ($staffs) use ($search) {
                $staffs->where('name', 'LIKE', "%$search%");
            })
            ->orWhere('code_number', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($register_reward);
    }

    public function searchOptions(Request $request)
    {
        $searchTitle = $request->keyOptionPersonalTitle;

        if ($searchTitle) {
            $register_reward = register_reward::query()
                ->select([
                    'id',
                    'user_id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'rank_staff_id',
                    'personal_title_id',
                    'reward_date',
                ])
                ->with('users',
                    'staffs',
                    'schools',
                    'units',
                    'ranks',
                    'personal_titles')
                ->where('personal_title_id', '=', $searchTitle)
                ->DataTablePaginate($request);
            return $this->item($register_reward);
        } else {
            $register_reward = register_reward::query()
                ->select([
                    'id',
                    'user_id',
                    'code_number',
                    'school_id',
                    'unit_id',
                    'rank_staff_id',
                    'personal_title_id',
                    'reward_date',
                ])
                ->with('users',
                    'staffs',
                    'schools',
                    'units',
                    'ranks',
                    'personal_titles')
                ->DataTablePaginate($request);
            return $this->item($register_reward);
        }
    }
}
