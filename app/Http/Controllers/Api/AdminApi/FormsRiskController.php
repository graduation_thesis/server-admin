<?php

namespace App\Http\Controllers;

use App\forms_risk;
use App\Http\Requests\FormsRiskCreateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormsRiskController extends AbstractApiController
{
    public function index()
    {
        return response()->json(forms_risk::get(), 200);
    }

    public function create(FormsRiskCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['forms_risk_id']                = $validatedData['forms_risk_id'];
        $payload['name']                         = $validatedData['name'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên hình thức rủi ro này');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu hình thức rủi ro
        $forms_risk = forms_risk::create($payload);
        DB::beginTransaction();

        try {
            $forms_risk->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm hình thức rủi ro thành công!');
            $this->setStatusCode(200);
            $this->setData($forms_risk);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($forms_risk_id)
    {
        return forms_risk::findOrFail($forms_risk_id);
    }

    public function update(FormsRiskCreateRequest $request, $forms_risk_id)
    {
        $validatedData = $request->validated();

        $forms_risk = forms_risk::query()->findOrFail($forms_risk_id);
        if (! $forms_risk) {
            $this->setMessage('Không có hình thức rủi ro');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên hình thức rủi ro
                $forms_risk->forms_risk_id                  = $validatedData['forms_risk_id'];
                $forms_risk->name                           = $validatedData['name'];

                $forms_risk->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($forms_risk);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        forms_risk::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $forms_risk = forms_risk::query()->get();
        foreach ($forms_risk->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }
}
