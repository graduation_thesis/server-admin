<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentCreateRequest;
use App\comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommentController extends AbstractApiController
{
    // TEST API MOBILE
    public function indexMobile()
    {
        return response()->json(comment::get(), 200);
    }

    // END TEST API MOBILE

    public function index(Request $request)
    {
        $comment = comment::query()
            ->select([
                'id',
                'user_id',
                'content',
                'feedback',
                'status',
                'created_at'
            ])
            ->with('users')
            ->where('status', '=', false)
            ->DataTablePaginate($request);

        return $this->item($comment);
    }

    public function create(CommentCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['user_id'] = $validatedData['user_id'];
        $payload['content'] = $request->content_new;
        $payload['feedback'] = 'Author';
        $payload['status'] = true;

        // Tạo và lưu bình luận
        $comment = comment::create($payload);
        DB::beginTransaction();

        try {
            $comment->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm bình luận thành công!');
            $this->setStatusCode(200);
            $this->setData($comment);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($user_id)
    {
        return comment::findOrFail($user_id);
    }

    public function update(CommentCreateRequest $request, $user_id)
    {
        $validatedData = $request->validated();

        $comment = comment::query()->findOrFail($user_id);
        if (! $comment) {
            $this->setMessage('Không có ý kiến phản hồi này');
            $this->setStatusCode(400);
            return $this->respond();
        }

        DB::beginTransaction();

        try {
            // Cập nhật tên danh mục
            $comment->user_id = $validatedData['user_id'];;
            $comment->content = $validatedData['content'];
            $comment->feedback = 'Administrator';
            $comment->status = true;

            $comment->save();
            DB::commit();

            // Trả về kết quả
            $this->setMessage('Phản hồi thành công');
            $this->setStatusCode(200);
            $this->setData($comment);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();

            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }

        return $this->respond();
    }

    public function remove($id)
    {
        comment::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $comment = comment::query()
            ->select([
                'id',
                'user_id',
                'content',
                'feedback',
                'status',
                'created_at'
            ])
            ->with('users')
//            ->where('status', '=', false)
            ->where('status', '=', false)
            ->where('content', 'LIKE', "%$search%")
            ->orWhere('user_id', 'LIKE', "%$search%")
            ->orWhere('feedback', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($comment);
    }
}
