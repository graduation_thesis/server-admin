<?php

namespace App\Http\Controllers;

use App\forms_of_discipline;
use App\Http\Requests\FormsOfDisciplineCreateRequest;
use Illuminate\Support\Facades\DB;

class FormsOfDisciplineController extends AbstractApiController
{
    public function index()
    {
        return response()->json(forms_of_discipline::get(), 200);
    }

    public function create(FormsOfDisciplineCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['forms_of_discipline_id'] = $validatedData['forms_of_discipline_id'];
        $payload['name'] = $validatedData['name'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên hình thức');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu cơ quan
        $forms_of_discipline = forms_of_discipline::create($payload);
        DB::beginTransaction();

        try {
            $forms_of_discipline->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm hình thức thành công!');
            $this->setStatusCode(200);
            $this->setData($forms_of_discipline);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($forms_of_discipline_id)
    {
        return forms_of_discipline::findOrFail($forms_of_discipline_id);
    }

    public function update(FormsOfDisciplineCreateRequest $request, $forms_of_discipline_id)
    {
        $validatedData = $request->validated();

        $forms_of_discipline = forms_of_discipline::query()->findOrFail($forms_of_discipline_id);
        if (! $forms_of_discipline) {
            $this->setMessage('Không có hình thức kỷ luật này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $forms_of_discipline->forms_of_discipline_id        = $validatedData['forms_of_discipline_id'];
                $forms_of_discipline->name                          = $validatedData['name'];

                $forms_of_discipline->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($forms_of_discipline);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        forms_of_discipline::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $forms_of_discipline = forms_of_discipline::query()->get();
        foreach ($forms_of_discipline->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }
}
