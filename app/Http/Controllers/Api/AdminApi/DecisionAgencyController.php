<?php

namespace App\Http\Controllers;

use App\decision_agency;
use App\Http\Requests\DecisionAgencyCreateRequest;
use Illuminate\Support\Facades\DB;

class DecisionAgencyController extends AbstractApiController
{
    //
    public function index()
    {
        return response()->json(decision_agency::get(), 200);
    }

    public function create(DecisionAgencyCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['decision_agency_id'] = $validatedData['decision_agency_id'];
        $payload['name'] = $validatedData['name'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên cơ quan');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu cơ quan
        $decision_agency = decision_agency::create($payload);
        DB::beginTransaction();

        try {
            $decision_agency->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm cơ quan thành công!');
            $this->setStatusCode(200);
            $this->setData($decision_agency);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($decision_agency_id)
    {
        return decision_agency::findOrFail($decision_agency_id);
    }

    public function update(DecisionAgencyCreateRequest $request, $decision_id)
    {
        $validatedData = $request->validated();

        $decision_agency = decision_agency::query()->findOrFail($decision_id);
        if (! $decision_agency) {
            $this->setMessage('Không có cơ quan quyết định này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật quyết định
                $decision_agency->decision_agency_id                  = $validatedData['decision_agency_id'];
                $decision_agency->name                                = $validatedData['name'];

                $decision_agency->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($decision_agency);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        decision_agency::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $decision_agency = decision_agency::query()->get();
        foreach ($decision_agency->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }
}
