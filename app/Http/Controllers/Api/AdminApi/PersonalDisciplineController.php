<?php

namespace App\Http\Controllers;

use App\discipline;
use App\Http\Requests\DisciplineCreateRequest;
use App\Http\Requests\PersonalDisciplineCreateRequest;
use App\staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PersonalDisciplineController extends AbstractApiController
{
    public function list(Request $request, $id)
    {
        $discipline = discipline::query()
            ->select([
                'id',
                'code_number',
                'forms_of_discipline_id',
                'decision_agency_id',
                'decision_id',
                'discipline_date',
                'created_at',
            ])
            ->with('staffs',
                'forms_of_disciplines',
                'decision_agencies',
                'decisions')
            ->where('code_number', '=', $id)
            ->DataTablePaginate($request);
        return $this->item($discipline);
    }

    public function create(PersonalDisciplineCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

            $payload['code_number']                     = $validatedData['code_number'];

            $getList = staff::query()->where('code_number', '=', $payload['code_number'])->get();

            $payload['school_id']                           = $getList[0]['school_id'];
            $payload['unit_id']                             = $getList[0]['unit_id'];
            $payload['team_id']                             = $getList[0]['team_id'];
            $payload['forms_of_discipline_id']              = $validatedData['forms_of_discipline_id'];
            $payload['decision_agency_id']                  = $validatedData['decision_agency_id'];
            $payload['decision_id']                         = $validatedData['decision_id'];
            $payload['discipline_date']                     = $validatedData['discipline_date'];

            // Tạo và lưu kỷ luật cá nhân
            $discipline = discipline::create($payload);
            DB::beginTransaction();

            try {
                $discipline->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm kỷ luật cá nhân thành công!');
                $this->setStatusCode(200);
                $this->setData($discipline);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        return $this->respond();
    }

    public function remove($id)
    {
        discipline::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $discipline = discipline::query()
            ->select([
                'id',
                'code_number',
                'school_id',
                'unit_id',
                'team_id',
                'forms_of_discipline_id',
                'decision_agency_id',
                'decision_id',
                'discipline_date',
                'created_at',
            ])
            ->with('staffs',
                'schools',
                'units',
                'teams',
                'forms_of_disciplines',
                'decision_agencies',
                'decisions')
            ->whereHas('forms_of_disciplines', function($forms_of_disciplines) use($search) {
                $forms_of_disciplines->where('name', 'LIKE', "%$search%");
            })->orWhereHas('decision_agencies', function($decision_agencies) use($search) {
                $decision_agencies->where('name', 'LIKE', "%$search%");
            })
            ->orWhere('decision_id', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($discipline);
    }
}
