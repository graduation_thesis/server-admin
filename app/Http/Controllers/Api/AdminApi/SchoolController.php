<?php

namespace App\Http\Controllers;

use App\Exports\SchoolsExport;
use App\Http\Requests\SchoolCreateRequest;
use App\school;
use DB;
use Illuminate\Http\Request;
use App\Imports\SchoolsImport;
use Maatwebsite\Excel\Facades\Excel;

class SchoolController extends AbstractApiController
{
    public function index()
    {
        return response()->json(school::get(), 200);
    }

    public function create(SchoolCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['school_id'] = $validatedData['school_id'];
        $payload['name'] = $validatedData['name'];
        $payload['build_date'] = $validatedData['build_date'];

        // Kiểm tra trùng tên
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên đơn vị trên cơ sở');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu đơn vị trên cơ sở
        $school = school::create($payload);
        DB::beginTransaction();

        try {
            $school->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm danh mục thành công!');
            $this->setStatusCode(200);
            $this->setData($school);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($school_id)
    {
        return school::findOrFail($school_id);
    }

    public function update(SchoolCreateRequest $request, $school_id)
    {
        $validatedData = $request->validated();

        $school = school::query()->findOrFail($school_id);
        if (! $school) {
            $this->setMessage('Không có đơn vị trên cơ sở này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $school->school_id = $validatedData['school_id'];
                $school->name = $validatedData['name'];
                $school->build_date = $validatedData['build_date'];

                $school->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($school);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        school::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    private function checkDuplicateName($name)
    {
        $school = school::query()->get();
        foreach ($school->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function import()
    {
        Excel::import(new SchoolsImport, request()->file('file'));

        return redirect('/')->with('success', 'All good!');
    }

    public function export()
    {
        return Excel::download(new SchoolsExport, 'school.xlsx');
    }
}
