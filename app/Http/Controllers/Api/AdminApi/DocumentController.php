<?php

namespace App\Http\Controllers;

use App\document;
use App\Http\Requests\DocumentCreateRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DocumentController extends AbstractApiController
{
    // TEST API MOBILE
    public function indexMobile()
    {
        return response()->json(document::get(), 200);
    }

    // END TEST API MOBILE

    public function index(Request $request)
    {
        $document = document::query()
            ->select([
                'id',
                'name',
                'document_file',
                'description',
            ])
            ->DataTablePaginate($request);

        return $this->item($document);
    }

    public function create(DocumentCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['name']                    = $validatedData['name'];
        $payload['document_file']           = $validatedData['document_file'];
        $payload['description']             = $validatedData['description'];


        // Kiểm tra trùng tên văn bản
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên văn bản');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu số quyết định
        $document = document::create($payload);
        DB::beginTransaction();

        try {
            $document->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm văn bản thành công!');
            $this->setStatusCode(200);
            $this->setData($document);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return document::findOrFail($id);
    }

    public function update(DocumentCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $document = document::query()->findOrFail($id);
        if (! $document) {
            $this->setMessage('Không có văn bản này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật văn bản
                $document->name                         = $validatedData['name'];
                $document->document_file                = $validatedData['document_file'];
                $document->description                  = $validatedData['description'];

                $document->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($document);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        document::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $document = document::query()->get();
        foreach ($document->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            $Carbon = new Carbon();
            $theTime = Carbon::now()->format('Y-m-d');
            $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
            $request->image->move(public_path('file/document'), $theImageName);
            return response()->json(['success' => 'có file'], 200);
        } else {
            return response()->json(['success' => 'chưa có file'], 200);
        }
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $document = document::query()
            ->select([
                'id',
                'name',
                'document_file',
                'description',
            ])
            ->where('name', 'LIKE', "%$search%")
            ->orWhere('description', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($document);
    }
}
