<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use Aritsan;
use Log;
use Storage;

class BackupController extends Controller
{
    public function create() {
        Artisan::call('backup:run', ['--only-db'=>true]);
        $output = Artisan::output();
        dump($output);
    }
}
