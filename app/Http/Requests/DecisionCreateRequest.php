<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DecisionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'decision_id'                               => 'required',
            'decision_file'                             => 'required',
            'sign_date'                                 => 'required|date',
            'received_date'                             => 'required|date',
            'description'                               => 'max:500',
        ];
    }

    public function messages()
    {
        return [
            'decision_id.required'                      => 'Bạn chưa nhập mã số quyết định',
            'decision_file.required'                    => 'Bạn chưa nhập file quyết định',
//            'decision_file.mimes'                       => 'File quyết định phải là định dạng .pdf',
            'sign_date.required'                        => 'Bạn chưa nhập ngày kí',
            'received_date.required'                    => 'Bạn chưa nhập ngày nhận',
            'description.max'                           => 'Trích yếu nội dung không được quá 500 kí tự',
        ];
    }
}
