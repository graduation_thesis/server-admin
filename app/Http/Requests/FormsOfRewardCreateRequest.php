<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormsOfRewardCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'forms_of_reward_id'                        => 'required',
            'name'                                      => 'required',
        ];
    }

    public function messages()
    {
        return [
            'forms_of_reward_id.required'               => 'Bạn chưa nhập mã hình thức',
            'name.required'                             => 'Bạn chưa nhập tên hình thức',
        ];
    }
}
