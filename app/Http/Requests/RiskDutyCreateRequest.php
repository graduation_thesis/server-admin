<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RiskDutyCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code_number'                              => 'required',
            'forms_risk_id'                            => 'required',
            'decision_agency_id'                       => 'required',
            'decision_id'                              => 'required',
            'risk_date'                                 => 'required',
        ];
    }

    public function messages()
    {
        return [
            'code_number.required'                     => 'Bạn chưa nhập cán bộ',
            'forms_risk_id.required'                   => 'Bạn chưa nhập mã hình thức rủi ro',
            'decision_agency_id.required'              => 'Bạn chưa nhập cơ quan quyết định',
            'decision_id.required'                     => 'Bạn chưa nhập số quyết định',
            'risk_date.required'                       => 'Bạn chưa nhập thời gian',
        ];
    }
}
