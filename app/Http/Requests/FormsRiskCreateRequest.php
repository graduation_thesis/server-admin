<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormsRiskCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'forms_risk_id'                             => 'required',
            'name'                                      => 'required',
        ];
    }

    public function messages()
    {
        return [
            'forms_risk_id.required'                    => 'Bạn chưa nhập mã hình thức rủi ro',
            'name.required'                             => 'Bạn chưa nhập tên hình thức rủi ro',
        ];
    }
}
