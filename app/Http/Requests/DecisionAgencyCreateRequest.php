<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DecisionAgencyCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'decision_agency_id'                        => 'required',
            'name'                                      => 'required',
        ];
    }

    public function messages()
    {
        return [
            'decision_agency_id.required'               => 'Bạn chưa nhập mã cơ quan',
            'name.required'                             => 'Bạn chưa nhập tên cơ quan',
        ];
    }
}
