<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RankStaffCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'rank_staff_id'                     => 'required',
            'name'                              => 'required',
        ];
    }

    public function messages()
    {
        return [
            'rank_staff_id.required'            => 'Bạn chưa nhập mã cấp bậc cán bộ',
            'name.required'                     => 'Bạn chưa nhập tên cấp bậc cán bộ',
        ];
    }
}
