<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalTitleCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'personal_title_id'                         => 'required',
            'name'                                      => 'required',
        ];
    }

    public function messages()
    {
        return [
            'personal_title_id.required'                => 'Bạn chưa nhập mã đơn vị trên cơ sở',
            'name.required'                             => 'Bạn chưa nhập tên đơn vị trên cơ sở',
        ];
    }
}
