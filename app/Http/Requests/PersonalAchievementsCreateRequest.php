<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalAchievementsCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code_number'                               => 'required',
//            'school_id'                                 => 'required',
//            'unit_id'                                   => 'required',
//            'team_id'                                   => 'required',
//            'personal_title_id'                         => 'nullable',
            'forms_of_reward_id'                        => 'nullable',
            'decision_agency_id'                        => 'required',
            'reward_date'                               => 'required',
            'decision_id'                               => 'required',
        ];
    }

    public function messages()
    {
        return [
            'code_number.required'                      => 'Bạn chưa nhập đơn vị trên cơ sở',
//            'school_id.required'                        => 'Bạn chưa nhập đơn vị cơ sở',
//            'unit_id.required'                          => 'Bạn chưa nhập đơn vị trực thuộc',
//            'team_id.required'                          => 'Bạn chưa nhập cán bộ',
            'decision_agency_id.required'               => 'Bạn chưa nhập cơ quan quyết định',
            'decision_id.required'                      => 'Bạn chưa nhập số quyết định',
            'forms_of_reward_id.required'               => 'Bạn chưa nhập hình thức',
            'reward_date.required'                      => 'Bạn chưa nhập ngày khen thưởng',
//            'personal_title_id.required'                => 'Bạn chưa nhập danh hiệu',
        ];
    }
}
