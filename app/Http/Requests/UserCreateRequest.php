<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'code_number'                       => 'required',
            'school_id'                         => 'required',
            'unit_id'                           => 'required',
            'username'                          => 'required',
            'password'                          => 'required',
            'role'                              => 'required'
        ];
    }

    public function messages()
    {
        return [
            'code_number.required'              => 'Bạn chưa nhập mã số của cán bộ',
            'school_id.required'                => 'Bạn chưa nhập mã đơn vị trên cơ sở',
            'unit_id.required'                  => 'Bạn chưa nhập mã đơn vị cơ sở',
            'username.required'                 => 'Bạn chưa nhập tài khoản',
            'password.required'                 => 'Bạn chưa nhập mật khẩu',
        ];
    }
}
