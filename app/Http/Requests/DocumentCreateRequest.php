<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'                                      => 'required',
            'document_file'                             => 'required',
            'description'                               => 'max:500',
        ];
    }

    public function messages()
    {
        return [
            'name.required'                             => 'Bạn chưa nhập tên văn bản',
            'document_file.required'                    => 'Bạn chưa nhập file văn bản',
            'description.max'                           => 'Trích yếu nội dung không được quá 500 kí tự',
        ];
    }
}
