<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StaffCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'full_name'                         => 'required|min:3|max:50',
            'birthday'                          => 'required|before:today',
            'code_number'                       => 'required',
            'sex'                               => 'required',
            // Bổ trợ tìm kiếm
            'school_id'                         => 'required|min:1',
            'unit_id'                           => 'required|min:1',
            'team_id'                           => 'nullable',
            'type_staff_id'                     => 'nullable',
            'position_staff_id'                 => 'nullable',
            'rank_staff_id'                     => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'full_name.required'                 => 'Bạn chưa nhập họ tên cán bộ',
            'birthday.required'                  => 'Bạn chưa nhập ngày sinh',
            'code_number.required'               => 'Bạn chưa nhập số hiệu cán bộ',
            'school_id.required'                 => 'Bạn chưa nhập đơn vị trên cơ sở',
            'unit_id.required'                   => 'Bạn chưa nhập đơn vị cơ sở',
//            'type_staff_id.required'             => 'Bạn chưa nhập loại cán bộ',
//            'position_staff_id.required'         => 'Bạn chưa nhập chức vụ cán bộ',
//            'rank_staff_id.required'             => 'Bạn chưa nhập cấp bậc chức vụ',
        ];
    }
}
