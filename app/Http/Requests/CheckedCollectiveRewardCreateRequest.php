<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckedCollectiveRewardCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'                                   => 'required',
            'approved_by'                               => 'nullable',
            'collective_title_id'                       => 'required',
            'school_id'                                 => 'required',
            'unit_id'                                   => 'required',
            'reward_date'                               => 'required',
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'                          => 'Bạn chưa nhập số tài khoản',
            'collective_title_id.required'              => 'Bạn chưa nhập danh hiệu',
            'school_id.required'                        => 'Bạn chưa nhập đơn vị trên cơ sở',
            'unit_id.required'                          => 'Bạn chưa nhập đơn vị cơ sở',
            'reward_date.required'                      => 'Bạn chưa nhập thời gian',
        ];
    }
}
