<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRewardCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'                                   => 'required',
            'code_number'                               => 'required',
            'personal_title_id'                         => 'required',
            'school_id'                                 => 'required',
            'unit_id'                                   => 'required',
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'                          => 'Bạn chưa nhập số tài khoản',
            'code_number.required'                      => 'Bạn chưa nhập số hiệu cán bộ',
            'personal_title_id.required'                => 'Bạn chưa nhập danh hiệu',
            'school_id.required'                        => 'Bạn chưa nhập đơn vị trên cơ sở',
            'unit_id.required'                          => 'Bạn chưa nhập đơn vị cơ sở',
        ];
    }
}
