<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SchoolCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'school_id'                         => 'required',
            'name'                              => 'required|max:100',
            'build_date'                        => 'required|date|before:today'
        ];
    }

    public function messages()
    {
        return [
            'school_id.required' => 'Bạn chưa nhập mã đơn vị trên cơ sở',
            'name.required' => 'Bạn chưa nhập tên đơn vị trên cơ sở',
            'name.max' => 'Tên đơn vị trên cơ sở không được quá 100 kí tự',
            'build_date.required' => 'Bạn chưa nhập ngày thành lập',
            'build_date.before' => 'Ngày thành lập phải nhỏ hơn ngày hiện tại',
        ];
    }
}
