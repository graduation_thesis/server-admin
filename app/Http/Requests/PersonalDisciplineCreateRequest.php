<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalDisciplineCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code_number'                              => 'required',
            'forms_of_discipline_id'                   => 'required',
            'decision_agency_id'                       => 'required',
            'decision_id'                              => 'required',
            'discipline_date'                          => 'required',
        ];
    }

    public function messages()
    {
        return [
            'code_number.required'                     => 'Bạn chưa nhập cán bộ',
            'forms_of_discipline_id.required'          => 'Bạn chưa nhập hình thức',
            'decision_agency_id.required'              => 'Bạn chưa nhập cơ quan quyết định',
            'decision_id.required'                     => 'Bạn chưa nhập số quyết định',
            'discipline_date.required'                 => 'Bạn chưa nhập thời gian kỷ luật',
        ];
    }
}
