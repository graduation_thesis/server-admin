<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    public function handle($request, Closure $next)
    {
//        header("Access-Control-Allow-Origin: *");
//        //ALLOW OPTIONS METHOD
//        $headers = [
//            'Access-Control-Allow-Methods' => 'POST,GET,OPTIONS,PUT,DELETE',
//            'Access-Control-Allow-Headers' => '*',
//        ];
//        if ($request->getMethod() == "OPTIONS"){
//            //The client-side application can set only headers allowed in Access-Control-Allow-Headers
//            return response()->json('OK',200,$headers);
//        }
//        $response = $next($request);
//        foreach ($headers as $key => $value) {
//            $response->header($key, $value);
//        }
//        return $response;


        $response = $next($request);

        $response->headers->set('Access-Control-Allow-Origin' , '*');
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization, X-Requested-With, Application');

        return $response;


//        $response = $next($request);
//
//        if ($response instanceof  Response) {
//            return $next($request)->header('Access-Control-Allow-Origin', '*')
//                ->header('Access-Control-Allow-Methods', 'GET,POST,PUT,OPTIONS,PATCH,DELETE,HEAD')
//                ->header('Access-Control-Allow-Headers', 'x-csrf-token,x-requested-with,content-type');
//        }
//        $response->headers->set('Access-Control-Allow-Origin', '*');
//        $response->headers->set('Access-Control-Allow-Methods', 'GET,POST,PUT,OPTIONS,PATCH,DELETE,HEAD');
//        $response->headers->set('Access-Control-Allow-Headers', 'x-csrf-token,x-requested-with,content-type');
//        return $response;



//        header("Access-Control-Allow-Origin: *");
//        //ALLOW OPTIONS METHOD
//        $headers = [
//            'Access-Control-Allow-Methods' => 'POST,GET,OPTIONS,PUT,DELETE',
//            'Access-Control-Allow-Headers' => 'Content-Type, X-Auth-Token, Origin, Authorization',
//        ];
//        if ($request->getMethod() == "OPTIONS") {
//            //The client-side application can set only headers allowed in Access-Control-Allow-Headers
//            return response()->json('OK', 200, $headers);
//        }
//        $response = $next($request);
//        foreach ($headers as $key => $value) {
//            $response->header($key, $value);
//        }
//        return $response;
        ///////////////////////=========================================/////////////
//        return $next($request)
//            ->header('Access-Control-Allow-Origin', '*')
//            ->header('Access-Control-Allow-Methods', 'POST,GET,OPTIONS,PUT,DELETE')
//            ->header('Access-Control-Max-Age', '86400')
//            ->header('Access-Control-Allow-Headers', 'Accept,Content-Type, Authorization, X-Requested-With');

//        return $next($request)
//            ->header('Access-Control-Allow-Origin', '*')
//            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
//            ->header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Token-Auth, Authorization');
//
    }
}
