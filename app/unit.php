<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Support\DataTablePaginate;

class unit extends Model
{
    use DataTablePaginate;

    protected $table = 'units';

    protected $fillable = [
        'school_id',
        'unit_id',
        'name',
        'build_date',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'school_id',
        'unit_id',
        'name',
        'build_date',
        'pending_remove',
    ];

    public function schools()
    {
        return $this->belongsTo(school::class, 'school_id','school_id');
    }

    // Quan hệ 1 nhiều (1 đơn vị có nhiều cán bộ)
    public function staffs()
    {
        return $this->hasMany(staff::class, 'unit_id', 'unit_id');
    }
}
