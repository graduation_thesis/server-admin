<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class forms_of_discipline extends Model
{
    //
    protected $fillable = [
        'forms_of_discipline_id',
        'name',
        'pending_remove',
    ];
}
