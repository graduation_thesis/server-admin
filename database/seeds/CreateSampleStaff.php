<?php

use App\staff;
use Illuminate\Database\Seeder;

class CreateSampleStaff extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $content = file_get_contents(__DIR__ . '/data/staff.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing staff ${idx}/${c}";

            if (empty($item['full_name'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $staff = staff::create($item);
            $staff->save();
        }

        echo PHP_EOL;
    }
}
