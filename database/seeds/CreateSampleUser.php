<?php

use App\User;
use Illuminate\Database\Seeder;

class CreateSampleUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'code_number'       => '123-456',
                'school_id'         => 'DHCSND',
                'unit_id'           => 'NVCB',
                'username'          => 'demoad',
                'password'          => bcrypt('123456'),
                'role'              => 'Administrator'
            ]
        );
        User::create(
            [
                'code_number'       => '123-789',
                'school_id'         => 'DHCSND',
                'unit_id'           => 'NVCB',
                'username'          => 'demo',
                'password'          => bcrypt('123456'),
                'role'              => 'author'
            ]
        );
        User::create(
            [
                'code_number'       => '123-123',
                'school_id'         => 'DHCSND',
                'unit_id'           => 'PL',
                'username'          => 'demo2',
                'password'          => bcrypt('123456'),
                'role'              => 'author'
            ]
        );
    }
}
