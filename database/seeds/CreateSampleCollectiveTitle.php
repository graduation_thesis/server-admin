<?php

use App\collective_title;
use Illuminate\Database\Seeder;

class CreateSampleCollectiveTitle extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $content = file_get_contents(__DIR__ . '/data/collective_title.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing collective title ${idx}/${c}";

            if (empty($item['name'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $collective_title = collective_title::create($item);
            $collective_title->save();
        }

        echo PHP_EOL;
    }
}
