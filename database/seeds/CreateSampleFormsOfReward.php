<?php

use Illuminate\Database\Seeder;
use App\forms_of_reward;

class CreateSampleFormsOfReward extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $content = file_get_contents(__DIR__ . '/data/forms_of_reward.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing forms of reward ${idx}/${c}";

            if (empty($item['name'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $forms_of_reward = forms_of_reward::create($item);
            $forms_of_reward->save();
        }

        echo PHP_EOL;
    }
}
