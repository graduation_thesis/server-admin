<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDecisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decisions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('decision_id')->unique()->comment('Số quyết định');

            $table->string('decision_file')->default('')->comment('File quyết định đính kèm');
            $table->date('sign_date')->comment('Ngày kí');
            $table->date('received_date')->comment('Ngày nhận');
            $table->text('description')->nullable()->comment('Trích yếu nội dung');

            $table->index('decision_id');

            $table->timestamp('pending_remove')->nullable()->comment('Thời gian xóa vĩnh viễn');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('decisions');
    }
}
