<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisciplinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disciplines', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('code_number')->comment('Số hiệu cán bộ, khóa ngoại');
            $table->string('school_id')->comment('Mã đơn vị trên cơ sở, khóa ngoại');
            $table->string('unit_id')->comment('Mã đơn vị cơ sở, khóa ngoại');
            $table->unsignedBigInteger('team_id')->nullable()->comment('Mã đơn vị thuộc cơ sở, khóa ngoại');
            $table->string('forms_of_discipline_id')->comment('Mã hình thức kỷ luật, khóa ngoại');
            $table->string('decision_agency_id')->comment('Mã cơ quan quyết định, khóa ngoại');
            $table->string('decision_id')->comment('Mã số quyết định, khóa ngoại');

            $table->date('discipline_date')->nullable()->comment('Ngày kỷ luật');
            $table->timestamp('pending_remove')->nullable()->comment('Thời gian xóa vĩnh viễn');

            $table->index(['code_number']);
            $table->index(['forms_of_discipline_id']);
            $table->index(['decision_agency_id']);
            $table->index(['decision_id']);

            $table->foreign('code_number')
                ->references('code_number')->on('staff')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('school_id')
                ->references('school_id')->on('schools')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('unit_id')
                ->references('unit_id')->on('units')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('team_id')
                ->references('id')->on('teams')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('forms_of_discipline_id')
                ->references('forms_of_discipline_id')->on('forms_of_disciplines')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('decision_agency_id')
                ->references('decision_agency_id')->on('decision_agencies')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('decision_id')
                ->references('decision_id')->on('decisions')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disciplines');
    }
}
