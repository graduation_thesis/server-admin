<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChangePersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('change_personals', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('school_id')->comment('Mã đơn vị trên cơ sở, khóa ngoại');
            $table->string('unit_id')->comment('Mã đơn vị cơ sở, khóa ngoại');
            $table->string('unit_id_changed')->comment('Mã đơn vị cơ sở mới chuyển đến');
            $table->unsignedBigInteger('team_id')->nullable()->comment('Mã đơn vị thuộc cơ sở, khóa ngoại');
            $table->string('type_staff_id')->comment('Mã loại cán bộ, khóa ngoại');
            $table->string('rank_staff_id')->comment('Mã cấp bậc, khóa ngoại');
            $table->string('position_staff_id')->comment('Mã chức vụ, khóa ngoại');

            $table->string('full_name')->comment('Họ tên cán bộ');
            $table->date('birthday')->comment('Ngày sinh');
            $table->string('code_number')->comment('Số hiệu cán bộ');
            $table->unsignedInteger('sex')->comment('Giới tính 0=Nam, 1=Nữ');

            $table->timestamp('pending_remove')->nullable()->comment('Thời gian xóa vĩnh viễn');

            $table->foreign('school_id')
                ->references('school_id')->on('schools')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('unit_id')
                ->references('unit_id')->on('units')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('unit_id_changed')
                ->references('unit_id')->on('units')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('team_id')
                ->references('id')->on('teams')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('type_staff_id')
                ->references('type_staff_id')->on('type_staffs')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('rank_staff_id')
                ->references('rank_staff_id')->on('rank_staffs')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('position_staff_id')
                ->references('position_staff_id')->on('position_staffs')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_personals');
    }
}
