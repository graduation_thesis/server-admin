<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChangeCollectivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('change_collectives', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('school_id')->comment('Mã đơn vị trên cơ sở, khóa ngoại');

            $table->string('unit_id')->comment('Mã đơn vị cơ sở, khóa ngoại');
            $table->string('unit_id_changed')->comment('Mã đơn vị cơ sở mới chuyển đến');

            $table->timestamp('pending_remove')->nullable()->comment('Thời gian xóa vĩnh viễn');

//            $table->foreign('school_id')
//                ->references('school_id')->on('schools')
//                ->onUpdate('cascade')
//                ->onDelete('cascade');
//
//            $table->foreign('unit_id')
//                ->references('unit_id')->on('units')
//                ->onUpdate('cascade')
//                ->onDelete('cascade');
//
//            $table->foreign('unit_id_changed')
//                ->references('unit_id')->on('units')
//                ->onUpdate('cascade')
//                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_collectives');
    }
}
