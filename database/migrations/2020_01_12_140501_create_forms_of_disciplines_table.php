<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsOfDisciplinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms_of_disciplines', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('forms_of_discipline_id')->unique()->comment('Mã hình thức kỷ luật');
            $table->string('name')->comment('Tên hình thức kỷ luật');

            $table->timestamp('pending_remove')->nullable()->comment('Thời gian xóa vĩnh viễn');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms_of_disciplines');
    }
}
