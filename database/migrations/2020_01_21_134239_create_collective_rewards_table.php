<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectiveRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collective_rewards', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('school_id')->comment('Mã đơn vị trên cơ sở, khóa ngoại');
            $table->string('unit_id')->comment('Mã đơn vị cơ sở, khóa ngoại');
            $table->string('forms_of_reward_id')->nullable()->comment('Mã hình thức, khóa ngoại');
            $table->string('decision_agency_id')->comment('Mã cơ quan quyết định, khóa ngoại');
            $table->string('decision_id')->comment('Mã số quyết định, khóa ngoại');

            $table->date('reward_date')->nullable()->comment('Ngày thi đua');
            $table->timestamp('pending_remove')->nullable()->comment('Thời gian xóa vĩnh viễn');

            $table->index(['unit_id']);
            $table->index(['decision_agency_id']);
            $table->index(['decision_id']);

            $table->foreign('unit_id')
                ->references('unit_id')->on('units')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('forms_of_reward_id')
                ->references('forms_of_reward_id')->on('forms_of_rewards')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('decision_agency_id')
                ->references('decision_agency_id')->on('decision_agencies')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('decision_id')
                ->references('decision_id')->on('decisions')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collective_rewards');
    }
}
