<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoveUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('move_units', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('staff_id')->comment('Mã cán bộ, khóa ngoại');
            $table->unsignedBigInteger('unit_id')->comment('Mã đơn vị, khóa ngoại');

            $table->date('date_of_transfer')->comment('Ngày chuyển công tác');

            $table->timestamp('pending_remove')->nullable()->comment('Thời gian xóa vĩnh viễn');

            $table->index(['staff_id']);
            $table->index(['unit_id']);

            $table->foreign('staff_id')
                ->references('id')->on('staff')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('unit_id')
                ->references('id')->on('units')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('move_units');
    }
}
