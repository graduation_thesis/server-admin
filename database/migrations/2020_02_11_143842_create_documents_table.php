<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->comment('Tên văn bản');
            $table->string('document_file')->default('')->comment('File văn bản đính kèm');
            $table->text('description')->nullable()->comment('Trích yếu nội dung');

            $table->timestamp('pending_remove')->nullable()->comment('Thời gian xóa vĩnh viễn');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
